#include "audio.h"
#include <QGuiApplication>

//audio::audio()
//{
//    sourceFile.setFileName("/audio/Taxim_-_Erinnerungen_an_Morgen_INTRO.wav");
//        sourceFile.open(QIODevice::ReadOnly);

//        QAudioFormat format;
//        // Set up the format, eg.
//        format.setSampleRate(8000);
//        format.setChannelCount(1);
//        format.setSampleSize(8);
//        format.setCodec("audio/pcm");
//        format.setByteOrder(QAudioFormat::LittleEndian);
//        format.setSampleType(QAudioFormat::UnSignedInt);

//        QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
//        if (!info.isFormatSupported(format)) {
//            qWarning() << "Raw audio format not supported by backend, cannot play audio.";
//            return;
//        }

//        audio = new QAudioOutput(format, this);
//        connect(audio, SIGNAL(stateChanged(QAudio::State)), this, SLOT(handleStateChanged(QAudio::State)));
//        audio->start(&sourceFile);
//}

Audio::Audio()
{
    player = new QMediaPlayer;

    // making and filling playlist with sounds
    playlist = new QMediaPlaylist;
    playlist->addMedia(QUrl("qrc:/audio/Taxim_-_Erinnerungen_an_Morgen_INTRO.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/meow1.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/meow5.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/playCard01.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/playCard02.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/playCard03.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/select1.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/select2.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/select3.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/select4.wav"));
    playlist->addMedia(QUrl("qrc:/audio/FX/shuffle1.wav"));
    playlist->addMedia(QUrl("qrc:/audio/announcer/attack2.wav"));
    playlist->addMedia(QUrl("qrc:/audio/announcer/defuse2.wav"));
    playlist->addMedia(QUrl("qrc:/audio/announcer/nope3.wav"));
    playlist->addMedia(QUrl("qrc:/audio/announcer/yourturn3.wav"));
    playlist->addMedia(QUrl("qrc:/audio/announcer/youwon.wav"));
    playlist->addMedia(QUrl("qrc:/audio/announcer/skip3.wav"));
    playlist->addMedia(QUrl("qrc:/audio/announcer/youlost.wav"));

    playlist->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

    loopplayer = new QMediaPlayer;

    // making and filling playlist with sound loops
    loopplaylist = new QMediaPlaylist;
    loopplaylist->addMedia(QUrl("qrc:/audio/Taxim_-_Erinnerungen_an_Morgen_LOOP.wav"));
    loopplaylist->addMedia(QUrl("qrc:/audio/POL-jungle-hideout-short.wav"));
    loopplaylist->addMedia(QUrl("qrc:/audio/POL-fortress-short.wav"));
    loopplaylist->addMedia(QUrl("qrc:/audio/POL-trial-and-error-short.wav"));
    loopplaylist->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);

    // setting playlists to right player
    player->setPlaylist(playlist);
    loopplayer->setPlaylist(loopplaylist);

    // set volume to 0
    player->setVolume(0);
    loopplayer->setVolume(0);

    connect(this, SIGNAL(playS()), player, SLOT(play()));
    connect(this, SIGNAL(volS(int)), player, SLOT(setVolume(int)));
    connect(this, SIGNAL(stopS()), player, SLOT(stop()));
    connect(this, SIGNAL(playL()), loopplayer, SLOT(play()));
    connect(this, SIGNAL(volL(int)), loopplayer, SLOT(setVolume(int)));
}

Audio::~Audio()
{
    delete player;
    delete loopplayer;
    delete playlist;
    delete loopplaylist;
}

void Audio::playSound(int index)
{
    playlist->setCurrentIndex(index);
    //player->play();
    emit playS();
}

void Audio::playLoop(int index)
{
    loopplaylist->setCurrentIndex(index);
    qDebug() << "[AUDIO]" << loopplaylist->currentMedia().canonicalUrl().toString();
    //loopplayer->play();
    emit playL();
}

void Audio::stopSound()
{
    //player->stop();
    emit stopS();
}

void Audio::stopLoop()
{
    loopplayer->stop();
}

void Audio::setVolume(int volFX, int volLOOP)
{
    //player->setVolume(volFX);
    //loopplayer->setVolume(volLOOP);
    emit volS(volFX);
    emit volL(volLOOP);
}

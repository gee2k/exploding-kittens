import QtQuick 2.0

Item {
    id: errormessage
    property alias msg: msg

    anchors.right: parent.right
    anchors.rightMargin: 120
    anchors.left: parent.left
    anchors.leftMargin: 120
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 120
    anchors.top: parent.top
    anchors.topMargin: 120

    Rectangle {
        id: background
        color: "#DD000000"
        anchors.fill: parent

        MouseArea {
            id: mouseBlock
            anchors.fill: parent
        }

        Text {
            id: msg
            color: "#ededed"
            text: qsTr("Text") + gameAdapter.translation
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            font.pixelSize: 24
        }


        Rectangle {
            id: exitrect
            x: 0
            width: 40
            height: 40
            color: "#99ffffff"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0

            Text {
                id: exittext
                color: "#d14141"
                text: "X"
                font.italic: false
                font.bold: true
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 22
            }

            MouseArea {
                id: clickArea
                anchors.fill: parent
                onClicked: errormessage.destroy()
            }
        }


    }


}

import QtQuick 2.0
import SoundTypes 1.0

Item {
    id: shuffle
    width: parent.height
    height: parent.height
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    Component.onCompleted: gameAdapter.slt_playSound(Sound.Shuffle)

    Card {
        id: nopeCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        image.source: "qrc:///nopeCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 0
    }

    Card {
        id: defuseCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        countRect.visible: false
        rotationAnim.running: true
        rotation: 30
    }

    Card {
        id: attackCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///attackCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 60
    }

    Card {
        id: favorCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///favorCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 90
    }

    Card {
        id: skipCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///skipCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 120
    }

    Card {
        id: seeTheFutureCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///seeTheFutureCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: parent.height * 0.4
    }

    Card {
        id: catsSchroedingerCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///catsSchroedingerCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 210
    }

    Card {
        id: mommaCatCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///mommaCatCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 240
    }

    Card {
        id: shyBladderCatCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///shyBladderCatCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 270
    }

    Card {
        id: zombieCatCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///zombieCatCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 300
    }

    Card {
        id: bikiniCatCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        //image.source: "qrc:///bikiniCatCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 330
    }

    Card {
        id: explodingKittenCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///ExplodingKittenCard"
        countRect.visible: false
        rotationAnim.running: true
        rotation: 360
    }

    Card {
        id: shuffleCard
        width: parent.height * 0.4
        height: parent.height * 0.8
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        image.source: "qrc:///shuffleCard"
        countRect.visible: false
        rotationAnim.running: true
        rotationAnim.onStopped: shuffle.destroy()
        rotation: 150
    }
}

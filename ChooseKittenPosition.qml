import QtQuick 2.0
import SoundTypes 1.0

Item {
    id: chooseKittenPosition
    property alias msg: msg

    anchors.right: parent.right
    anchors.rightMargin: 120
    anchors.left: parent.left
    anchors.leftMargin: 120
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 120
    anchors.top: parent.top
    anchors.topMargin: 120

    Rectangle {
        id: background
        color: "#DD000000"
        anchors.fill: parent

        Text {
            id: msg
            color: "#ffffff"
            text: qsTr("Choose where to place the Kitten:") + gameAdapter.translation
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.top: parent.top
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
        }

        Rectangle {
            id: posWrapper
            color: "#00000000"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: msg.bottom
            anchors.topMargin: 0

            Rectangle {
                id: posBG
                y: 0
                width: 360
                height: childrenRect.height
                color: "#000000"
                border.color: "#ffffff"
                anchors.right: parent.right
                anchors.rightMargin: 140
                anchors.left: parent.left
                anchors.leftMargin: 140
                anchors.verticalCenter: parent.verticalCenter

                TextInput {
                    id: pos
                    y: 216
                    height: 22
                    color: "#ffffff"
                    text: {gameAdapter.amountOfCardsInDrawPile / 2}
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    horizontalAlignment: Text.AlignHCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 20
                    onTextChanged: {
                        if(pos.text < 0) {
                            pos.text = 0;
                        }
                        if(pos.text > gameAdapter.amountOfCardsInDrawPile) {
                            pos.text = gameAdapter.amountOfCardsInDrawPile;
                        }
                    }
                }
            }

            Rectangle {
                id: createButton
                height: 22
                color: "#ffffff"
                anchors.right: parent.right
                anchors.rightMargin: 140
                anchors.left: parent.left
                anchors.leftMargin: 140
                anchors.top: posBG.bottom
                anchors.topMargin: 10

                Text {
                    id: text1
                    text: qsTr("Set position") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    font.pixelSize: 20
                }

                MouseArea {
                    id: mouseArea1
                    anchors.fill: parent
                    onClicked: {
                        gameAdapter.addKittenIntoDrawPileAtPosition(pos.text);
                        chooseKittenPosition.destroy();
                        gameAdapter.slt_playLoop(Sound.InGameLoop);
                    }
                }
            }
        }

    }


}

#ifndef GAMEADAPTER_H
#define GAMEADAPTER_H
#include <QObject>
#include <QDebug>
#include "gameplay.h"
#include <thread>
#include <memory>
#include "client.h"
#include "server.h"
#include <QList>
#include "dbhelper.h"
#include "audio.h"
#include <QTranslator>

/**
 * @brief The gameAdapter class is the center connector from the GUI to the backend. It connectts GUI to network and gameplay
 */
class gameAdapter : public QObject
{
    Q_OBJECT

    //Menu
    /**
     * @brief qmlReadIpAdress is the ip address which comes from the text input in GUI
     */
    Q_PROPERTY(QString qmlReadIpAdress READ getIpText WRITE setIpText NOTIFY ipAdressInTextboxChanged)

    /**
     * @brief translation is used as a workaround to update translation in qml in-game
     */
    Q_PROPERTY(QString translation MEMBER m_trans NOTIFY languageChanged)


    //ingame
    /**
     * @brief qmlPlayerHandCards is the vector of cards the local player has on his deck
     */
    Q_PROPERTY(QVector<AbstractCard::CardTypes> qmlPlayerHandCards READ getPlayerHandCards NOTIFY playerHandCardsChanged)  //has all cards the player has on its hand

    /**
     * @brief qmlDrawnCard is the last drawn card from the local player
     */
    Q_PROPERTY(AbstractCard::CardTypes qmlDrawnCard MEMBER m_drawCard NOTIFY qmlDrawnCardChanged)                       //the last card the local player has drawn

    /**
     * @brief qmlPlayedCard is the last played card by a player
     */
    Q_PROPERTY(AbstractCard::CardTypes qmlPlayedCard MEMBER m_cardPlayed NOTIFY qmlPlayedCardChanged)                   //the last played card the last player played

    /**
     * @brief qmlIsHost indicates if the user acts as a server or not
     */
    Q_PROPERTY(bool qmlIsHost MEMBER m_isHost)

    /**
     * @brief qmlNoping indicates if the local player is in the nope state or not
     */
    Q_PROPERTY(bool qmlNoping MEMBER m_noping)

    /**
     * @brief qmlMatchRunning indicates if the player is in a match or not
     */
    Q_PROPERTY(bool qmlMatchRunning MEMBER m_matchIsRunning)

    /**
     * @brief amountOfCardsInDrawPile is the amount of cards in the drawpile
     */
    Q_PROPERTY(unsigned int amountOfCardsInDrawPile READ getAmountOfCardsInDrawPile WRITE setAmountOfCardsInDrawPile NOTIFY amountOfCardsInDrawPileChanged)

    /**
     * @brief localPlayersTurnBegun indicates if the local player is on his turn or not
     */
    Q_PROPERTY(bool localPlayersTurnBegun MEMBER m_localPlayerTurn NOTIFY sig_localPlayersTurnBegun)

    /**
     * @brief qmlAmountAttackCards is the amount of attack cards in the player deck
     */
    Q_PROPERTY(int qmlAmountAttackCards READ getAmountAttackCards NOTIFY qmlAmountAttackCardsChanged)

    /**
     * @brief qmlAmountDefuseCards is the amount of defuse cards in the player deck
     */
    Q_PROPERTY(int qmlAmountDefuseCards READ getAmountDefuseCards NOTIFY qmlAmountDefuseCardsChanged)

    /**
     * @brief qmlAmountExplosiveKittenCards is the amount of explosiv kitten cards in the player deck
     */
    Q_PROPERTY(int qmlAmountExplosiveKittenCards READ getAmountExplosiveKittenCards NOTIFY qmlAmountExplosiveKittenCardsChanged)

    /**
     * @brief qmlAmountFavorCards is the amount of favor cards in the player deck
     */
    Q_PROPERTY(int qmlAmountFavorCards READ getAmountFavorCards NOTIFY qmlAmountFavorCardsChanged)

    /**
     * @brief qmlAmountNopeCards is the amount of nope cards in the player deck
     */
    Q_PROPERTY(int qmlAmountNopeCards READ getAmountNopeCards NOTIFY qmlAmountNopeCardsChanged)

    /**
     * @brief qmlAmountSeeTheFutureCards is the amount of see the future cards in the player deck
     */
    Q_PROPERTY(int qmlAmountSeeTheFutureCards READ getAmountSeeTheFutureCards NOTIFY qmlAmountSeeTheFutureCardsChanged)

    /**
     * @brief qmlAmountShuffleCards is the amount of shuffle cards in the player deck
     */
    Q_PROPERTY(int qmlAmountShuffleCards READ getAmountShuffleCards NOTIFY qmlAmountShuffleCardsChanged)

    /**
     * @brief qmlAmountSkipCards is the amount of skip cards in the player deck
     */
    Q_PROPERTY(int qmlAmountSkipCards READ getAmountSkipCards NOTIFY qmlAmountSkipCardsChanged)

    /**
     * @brief qmlAmountBikiniCatCards is the amount of bikini cat cards in the player deck
     */
    Q_PROPERTY(int qmlAmountBikiniCatCards READ getAmountBikiniCatCards NOTIFY qmlAmountBikiniCatCardsChanged)

    /**
     * @brief qmlAmountCatsSchroedingerCards is the amount of cats schroedinger cards in the player deck
     */
    Q_PROPERTY(int qmlAmountCatsSchroedingerCards READ getAmountCatsSchroedingerCards NOTIFY qmlAmountCatsSchroedingerCardsChanged)

    /**
     * @brief qmlAmountMommaCatCards is the amount of momma cat cards in the player deck
     */
    Q_PROPERTY(int qmlAmountMommaCatCards READ getAmountMommaCatCards NOTIFY qmlAmountMommaCatCardsChanged)

    /**
     * @brief qmlAmountShyBladderCards is the amount of shy bladder cat cards in the player deck
     */
    Q_PROPERTY(int qmlAmountShyBladderCards READ getAmountShyBladderCards NOTIFY qmlAmountShyBladderCardsChanged)

    /**
     * @brief qmlAmountZombieCatCards is the amount of zombie cat cards in the player deck
     */
    Q_PROPERTY(int qmlAmountZombieCatCards READ getAmountZombieCatCards NOTIFY qmlAmountZombieCatCardsChanged)

public:
    /**
     * @brief gameAdapter the standart constructor
     */
    gameAdapter();

    ~gameAdapter();

    /**
      * @brief selectLanguage changes the UI language
      * @param lang the language to be set to
      */
    Q_INVOKABLE void selectLanguage(int lang);

    /**
     * @brief method to be called from the GUI to join a game
     * */
    Q_INVOKABLE void joinGame();

    /**
     * @brief method to be called from the GUI to host a game
     * */
    Q_INVOKABLE void hostGame();

    /**
     * @brief method to be called from the GUI to start a game after lobby is filled
     * */
    Q_INVOKABLE void startHostedGame();

    /**
     * @brief method to be called from the GUI to send the cards from the client to the server (via buffer)
     * */
    Q_INVOKABLE void playCards();

    /**
     * @brief Helper Method to the GUI to read the database Helper
     * */
    Q_INVOKABLE QString getActiveProfile();

    /**
     * @brief Helper Method to the GUI to change the playername
     * */
    Q_INVOKABLE void changePlayerName(QString name);

    /**
     * @brief Helper Method to the GUI to get the player stats
     * */
    Q_INVOKABLE QVariantList getActiveProfileStats();

    /**
     * @brief Helper Method to the GUI to create the player profile for stats
     * */
    Q_INVOKABLE bool createLocalProfile(QString name);

    /**
     * @brief called when a client wants to leave the game
     * */
    Q_INVOKABLE void clientLeaveGame();

    /**
     * @brief vectorToQVariant is a static method to convert a vector to a QVariant
     * @param cards is the vector which has to be converted
     * @return the resulting QVariant
     */
    static QVariant vectorToQVariant(std::vector<AbstractCard::CardTypes> cards);

    /**
     * @brief addKittenIntoDrawPileAtPosition is called via GUI to add the Kitten inside the drawPile. if the position is valid it will send it to the server otherwise there is a new signal to the gui
     * @param position between 0 and drawpile size (+1 ?)
     */
    Q_INVOKABLE void addKittenIntoDrawPileAtPosition(QString position);

    /**
     * @brief getSettings gets a vector of settings and returns it as as QVariant
     * @return list of settings in a QVariant
     */
    Q_INVOKABLE QVariant getSettings();

    /**
     * @brief saveSettings sets new Settings and saves them in the database
     */
    Q_INVOKABLE void saveSettings(int lang, int volFX, int volMUSIC, int fullscreen, QString name);


public slots:
    //audio
    /**
     * @brief slt_playLoop plays a sound out of the loop list
     * @param enm the slot of the sound
     */
    Q_INVOKABLE void slt_playLoop(int enm);

    /**
     * @brief slt_playSound plays a sound
     * @param enm the slot of the sound
     */
    Q_INVOKABLE void slt_playSound(int enm);

    /**
     * @brief slt_stopSound stop playing
     */
    Q_INVOKABLE void slt_stopSound();

    //server

    /**
     * @brief Slot gets called from network when a new player connected to the server
     * the player gets added to the playerList
     * @param name of the player
     * @param ip of the player
     * */
    void slt_newPlayerConnectedToHost(QString name, QString ip); //can be accessed from Network //connected from Server


    /**
     * @brief Slot gets called when a player sends its "ready" signal to the host
     * @param ip the ip of the player who is ready
     * */
    void sltCheckPlayerReady(QString ip);


    //server - host
    /**
     * @brief slt_kickPlayer kicks a player via GUI from the host
     * @param ip the ip of the player to be kicked
     */
    void slt_kickPlayer(QString ip);

    /**
     * @brief slt_receivedPlayerLeft is received from server to remove it from the player lists
     * @param ip of the player who left
     */
    void slt_receivedPlayerLeft(QString ip);

    /**
     * @brief slt_clientDisconnectedReceived is called from the network when a client has disconnected from the server
     * @param ip of the client who left the game or lobby
     */
    void slt_clientDisconnectedReceived(QString ip);

    //client
    /**
     * @brief Slot gets called from network when all are ready and the game is about to start
     * it first checks if the local player is still ready and initiates then the gameloop
     * */
    void slt_networkReceivedGameStarting(); //connected from client

    /**
     * @brief slt_networkReceivedGameEnded gets called when the host send the game finished
     */
    void slt_networkReceivedGameEnded();

    /**
     * @brief slt_clientReceivedIP is called from client when the server sent the clients public ip
     * @param ip of the client (public)
     */
    void slt_clientReceivedIP(QString ip);

    /**
     * @brief slot for client network when new player list arrives. It checks for every new player (igors wish) and announces it to the gui
     * @param newplayerList list with all players
     */
    void slt_receivedNewPlayerlist(std::vector<publicPlayer*>* newplayerList);  //connected from client

    /**
     * @brief slt_clientConnected, connected to the clients connect signal to check if a connection was successful
     */
    void slt_clientConnected(); //connected from client

    /**
     * @brief slt_clientDisconnected, connected to the clients disconnect signal to check if a connection was successfully disconnected
     */
    void slt_clientDisconnected();  //connected from Client

    /**
     * @brief slt_playedCardsReceived holds the cards which where played by an other player to show it to the GUI
     * @param ip of the player who played
     * @param cards the ones who where played
     */
    void slt_playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards);

    /**
     * @brief slt_cardsReceived contains the cards for the player (can be start deck or drawn cards)
     * @param cards the player got
     */
    void slt_cardsReceivedForPlayerHandCards(std::vector<AbstractCard::CardTypes> cards);

    /**
     * @brief slt_networkReceivedActivePlayer is called from the client when a new active player is determined by the server, it activates the local player if it is its turn and deactivates it if not
     * @param ip of the player whos turn is
     */
    void slt_networkReceivedActivePlayer(QString ip);

    /**
     * @brief slt_networkChangedState is received from the network when a state change is emitted, if its nopeing phase and local player is not at turn, he gets activated to nope
     * @param state in which the game switched
     */
    void slt_networkChangedState(BaseState::playStates state);

    /**
     * @brief slt_seeTheFutureCardsReceived contains 0 to 3 cards for peeking at the top max 3 drawpile cards
     * @param cards containing the top 3 max cards
     */
    void slt_seeTheFutureCardsReceived(std::vector<AbstractCard::CardTypes> cards);

    /**
     * @brief slt_shuffleReceived is connected to the client network to receive shuffle deck calls from the host
     */
    void slt_shuffleReceived();   //connected to client

    /**
     * @brief slt_receivedNewCoPlayerCardAmount is connected to the client when other players amount of cards on hand change
     * @param ip the player who got a new card amount
     * @param amount cards of an coplayer1
     */
    void slt_receivedNewCoPlayerCardAmount(QString ip, uint amount);   //connect to client

    /**
     * @brief slt_receivedNewAmountForDrawPile connected via client network when new amount is received
     * @param newAmount the amount of cards
     */
    void slt_receivedNewAmountForDrawPile(unsigned int newAmount);  //connected to client

    /**
     * @brief slt_receivedPlayerStats writes player statistics to database after receiving it from host
     * @param playerStatistics own statistics received from server
     * @param playerList list of player who was played against in the previous round
     */
    void slt_receivedPlayerStats(QMap<int, unsigned int> playerStatistics, QString playerList);

    /**
     * @brief slt_receivedGameStats transports the running game's statistics when it's over to gui
     * @param gameBestOfStatistics the running game's statistics
     */
    void slt_receivedGameStats(QMap<int, QMap<QString, unsigned int> > gameBestOfStatistics);

    //ingame client
    /**
     * @brief slt_addCardToBePlayed adds a card send from the GUI to the "playedCards" Buffer
     * @param cardEnum which is of type card which has been played
     */
    void slt_addCardToBePlayed(int cardEnum);   //add card to buffer

    /**
     * @brief slt_removeCardToBePlayed removes a card from the "playedCards" Buffer and re adds it to the players hand
     * @param cardEnum which is of type card
     */
    void slt_removeCardToBePlayed(int cardEnum);//remove card from buffer

    /**
     * @brief slt_clearCardBuffer puts the cards back on the players hands
     */
    void slt_clearCardBuffer();

    /**
     * @brief slt_progressChanged called on time left change in state at host
     * @param percent of the time remaining
     */
    void slt_progressChanged(double percent);

    //global

    /**
     * @brief slt_errorMessage is called when a message is received from server
     * @param message the content of the message
     */
    void slt_errorMessage(QString message);

    /**
     * @brief slt_playerLost is called when a loosing player was received from server
     * @param ip the ip of the player who have lost
     */
    void slt_playerLost(QString ip);

    /**
     * @brief slt_playerWon is called when the winner was received from server
     * @param ip the ip of the winner
     */
    void slt_playerWon(QString ip);

signals:
    //qml QProperties

    /**
     * @brief languageChanged is emitted when the language was switched
     */
    void languageChanged();
    /**
     * @brief ipAdressInTextboxChanged via QProperty, emitted when the ip is changed in the join menu ip text box
     */
    void ipAdressInTextboxChanged();        //to UI Signal

    /**
     * @brief playerHandCardsChanged via QProperty, emitted when the amount of cards on the players hand change
     */
    void playerHandCardsChanged();          //to UI Signal for QProperty

    /**
     * @brief qmlDrawnCardChanged via QProperty, emitted when the drawncard property changes which holds the last drawn card by the local player
     */
    void qmlDrawnCardChanged();

    /**
     * @brief qmlPlayedCardChanged via QProperty, holds the last played card on game
     */
    void qmlPlayedCardChanged();

    /**
     * @brief amountOfCardsInDrawPileChanged is emitted when the amount of cards in the drawpile ingame changes
     */
    void amountOfCardsInDrawPileChanged();


    //following for refreshing the player deck in qml
    /**
     * @brief qmlAmountAttackCardsChanged emitted when attack cards amount change in player hand
     */
    void qmlAmountAttackCardsChanged();

    /**
     * @brief qmlAmountDefuseCardsChanged emitted when defuse cards amount change in player hand
     */
    void qmlAmountDefuseCardsChanged();

    /**
     * @brief qmlAmountExplosiveKittenCardsChanged emitted when explosiveKitten cards amount change in player hand
     */
    void qmlAmountExplosiveKittenCardsChanged();

    /**
     * @brief qmlAmountFavorCardsChanged emitted when favor cards amount change in player hand
     */
    void qmlAmountFavorCardsChanged();

    /**
     * @brief qmlAmountNopeCardsChanged emitted when nope cards amount change in player hand
     */
    void qmlAmountNopeCardsChanged();

    /**
     * @brief qmlAmountSeeTheFutureCardsChanged emitted when seeTheFuture cards amount change in player hand
     */
    void qmlAmountSeeTheFutureCardsChanged();

    /**
     * @brief qmlAmountShuffleCardsChanged emitted when shuffle cards amount change in player hand
     */
    void qmlAmountShuffleCardsChanged();

    /**
     * @brief qmlAmountSkipCardsChanged emitted when skip cards amount change in player hand
     */
    void qmlAmountSkipCardsChanged();

    /**
     * @brief qmlAmountBikiniCatCardsChanged emitted when bikini cards amount change in player hand
     */
    void qmlAmountBikiniCatCardsChanged();

    /**
     * @brief qmlAmountCatsSchroedingerCardsChanged emitted when catsSchroedinger cards amount change in player hand
     */
    void qmlAmountCatsSchroedingerCardsChanged();

    /**
     * @brief qmlAmountMommaCatCardsChanged emitted when MommaCat cards amount change in player hand
     */
    void qmlAmountMommaCatCardsChanged();

    /**
     * @brief qmlAmountShyBladderCardsChanged emitted when shyBladder cards amount change in player hand
     */
    void qmlAmountShyBladderCardsChanged();

    /**
     * @brief qmlAmountZombieCatCardsChanged emitted when ZombieCat cards amount change in player hand
     */
    void qmlAmountZombieCatCardsChanged();

    //client
    /**
     * @brief signal to tell the GUI when a new client connected to be shown in the lobby
     * */
    void sig_newPlayerInLobby(QString name, QString ip);

    /**
     * @brief signal to tell the GUI when a client disconnected to be removed from the "visual" lobby
     * */
    void sig_playerLeftLobby(QString name, QString ip);

    //client ingame

    /**
     * @brief gameStatsReceived is emitted when the game ends and the overall statistics should be displayed
     * @param gameStats the overall game statistics
     */
    void gameStatsReceived(QVariant gameStats);

    /**
     * @brief sig_playedCards is emitted when new cards arrive which are played by a player. It tells the GUI which player has played which cards during a round
     * @param name the players name who played
     * @param cards the cards the player played
     */
    void sig_playedCards(QString name, QVariant cards);

//    /**
//     * @brief sig_cardInHandChanged is emitted everytime the amount of cards in the players hand cahnges to refresh the GUI
//     */
//    void sig_cardInHandChanged();

    /**
     * @brief sig_localPlayersTurnBegun emitted to the GUI to notify
     */
    void sig_localPlayersTurnBegun();

    /**
     * @brief sig_localPlayersTurnEnded emitted to the GUI to notify
     */
    void sig_localPlayersTurnEnded();

    /**
     * @brief sig_coplayersCardsOnHand emitted to the GUI to refresh the counter of amounts on Cards on the remote players hands
     * @param ip of the remote player
     * @param amount of cards it has
     */
    void sig_coplayersCardsOnHand(QString ip, QString amount);

    /**
     * @brief sig_explodingKittenDrawn, emitted when the local player draws a kitten to the GUI
     */
    void sig_explodingKittenDrawn();

    /**
     * @brief sig_drawPileShuffled, emitted to the GUI when the drawPile has been shuffled
     */
    //void sig_drawPileShuffled();

    /**
     * @brief sig_seeTheFuture is emitted to the GUI when the See The Future card was used to see the 3 top cards on the drawPile (received by the server)
     * @param cards (0 to 3)
     */
    void sig_seeTheFuture(QVariant cards);

    /**
     * @brief sig_shuffle is emitted to the GUIwhen a shuffle card was played
     */
    void sig_shuffle();

    /**
     * @brief sig_placeKitten is emitted when the GUI should show the place kitten ins drawpile dialog
     */
    void sig_placeKitten();

    /**
     * @brief sig_sendServerKittenPositionInDrawPile is emitted to the client to send the position to the server
     * @param position of the cat to be in the drawPile
     */
    void sig_sendServerKittenPositionInDrawPile(int position);

    /**
     * @brief sig_gamePlayPlayerLeftGame is emitted to the gameplay to hold the game and remove the player from the game
     * @param ip of the player who left
     */
    void sig_informGameplayPlayerLeftGame(QString ip);

    /**
     * @brief newActivePlayer is emitted every time the active player changes
     * @param ip of the active player
     */
    void newActivePlayer(QString ip);

    /**
     * @brief sig_progressChanged emitted when the client gets a message with the remaining time to play in a state
     * @param percent the progress percentage from 0 to 1
     */
    void sig_progressChanged(double percent);

    //server
    /**
     * @brief sigSendUpdatedPlayerList, when a new player connected the new player list is emitted to the server component to send it to all clients to refresh their player lists
     * @param localPublicPlayerList list containing the player
     */
    void sigSendUpdatedPlayerList(std::vector<publicPlayer*>* localPublicPlayerList); //connected to Server and GP

    /**
     * @brief sig_SendCardsPlayedToNetwork, cards played by a player (and received by the network, processed by the game) are send to all clients to refresh their GUI
     * @param cards who where played
     */
    void sig_SendCardsPlayedToNetwork(std::vector<AbstractCard::CardTypes> cards);  //connected to Client

    /**
     * @brief sig_StartHostedGame is emitted when the "Host" starts a game to the GUI which shows the appropriate Screen
     */
    void sig_StartHostedGame(); //connected to Server

    /**
     * @brief sig_createGameForm is emitted when a game starts tho show the ingame GUI
     */
    void sig_createGameForm();

    /**
     * @brief sig_announceGPPlayerLeftGame is emitted to the server side gameloop that a player left
     * @param ip of the player who left
     */
    void sig_announceGPPlayerLeftGame(QString ip);

    //global
    /**
     * @brief sig_errorMessage to emit error messages
     * @param message containing the error
     */
    void sig_errorMessage(QString message);

    /**
     * @brief sig_dbHelperInit is emitted when the db helper is ready to go
     */
    void sig_dbHelperInit();

    /**
     * @brief sig_playerLost is emitted when a player looses the game
     * @param ip the loosers ip
     */
    void sig_playerLost(QString ip);

    /**
     * @brief sig_gameEnded is emitted to the gui when the match has ended
     */
    void sig_gameEnded();

private:

    /**
     * @brief translator qt translator
     */
    QTranslator* translator;

    /**
     * @brief m_trans workaround used for translation in qml
     */
    QString m_trans;

    /**
     * @brief m_connectToIpAdressText is the internal string holding the ip to connect the client to
     */
    QString m_connectToIpAdressText;

    /**
     * @brief activeState holds the active gameState
     */
    BaseState::playStates activeState;

    /**
     * @brief m_gameStarted is set when a host game is started so no second can be. its set to false when the game ends.
     */
    bool m_gameStarted = false;

    /**
     * @brief m_isHost holds info if this is a host or a client
     */
    bool m_isHost = false;

    /**
     * @brief m_matchIsRunning holds info about client game in progress
     */
    bool m_matchIsRunning = false;

    /**
     * @brief m_localPlayerTurn
     */
    bool m_localPlayerTurn = false;

    /**
     * @brief m_guiActive is enabled when player is activated to play a card
     */
    bool m_guiActive = false;

    /**
     * @brief m_noping is used to decide its a noping round or not
     */
    bool m_noping = false;

    /**
     * @brief m_endedPlayCardState to check if cards where send during state
     */
    bool m_endedPlayCardState = false;

    /**
     * @brief m_amountOfCardsInDrawPile holds the info about the cards in the drawPile via QProperty
     */
    unsigned int m_amountOfCardsInDrawPile = 0;

    /**
     * @brief hostgp holds the gamePlay Object when host
     */
    hostGameplay* hostgp = NULL;                            //remember to delete !

    /**
     * @brief hostPrivatePlayerList list of players for the host
     */
    std::vector<privatePlayer*>* hostPrivatePlayerList;     //remember to delete !

    /**
     * @brief localPublicPlayerList list of players for the clients with less information
     */
    std::vector<publicPlayer*>* localPublicPlayerList;      //remember to delete !

    /**
     * @brief localPublicPlayerList list of players for the clients with less information
     */
    std::vector<publicPlayer*>* publicPlayerListBuffer;      //remember to delete !

    /**
     * @brief server the server component for the host
     */
    Server* server = NULL;                                  //remember to delete !

    /**
     * @brief client component for the clients
     */
    Client* client = NULL;                                  //remember to delete !

    /**
     * @brief m_clientConnected to check if the client is connected, is switched via Signal from client network component
     */
    bool m_clientConnected = false;

    /**
     * @brief localPlayer object holding the card information for the playing client
     */
    privatePlayer* localPlayer;

    /**
     * @brief dbHelper for statistics
     */
    DBHelper* dbHelper;

    /**
     * @brief audio component
     */
    Audio* audio;

    /**
     * @brief m_playerHandCardsForGUI holds the cards the localPlayer has in his hands
     */
    QVector<AbstractCard::CardTypes> m_playerHandCardsForGUI;

    /**
     * @brief cardsToBePlayedBuffer holds the cards bevor sending them to the server on play
     */
    std::vector<AbstractCard::CardTypes> cardsToBePlayedBuffer;        //buffer for cards to be sent

    /**
     * @brief m_drawCard holds the information about the last drawn card of the local player for the GUI via QProperty
     */
    AbstractCard::CardTypes m_drawCard = AbstractCard::ERRORCARD;

    /**
     * @brief m_cardPlayed holds the information about the last played card for the GUI via QProperty
     */
    AbstractCard::CardTypes m_cardPlayed = AbstractCard::ERRORCARD;

    //server

    /**
     * @brief removeFromPlayerList removes a player from the server side playerlist
     * @param ip of the player to remove
     */
    void removeFromPlayerList(QString ip);

    /**
     * @brief sendUpdatedPlayerList, creates a public player list containing less sensitiv information from the hosts full playerlist to send it to the clients
     */
    void sendUpdatedPlayerList();

    /**
     * @brief lobby
     */
//    void lobby();
//    void setupClient();

    /**
     * @brief cleanUp is called on the host machine, when the game ended
     */
    void cleanUp();

    //client

    /**
     * @brief startJoinedGame is called when the signal from the server signaled that the game was started on the host
     */
    void startJoinedGame();

    /**
     * @brief assignNewLocalPublicPlayerList replaced the client player list with the new received from the host
     * @param newLocalPlayerList
     */
    void assignNewLocalPublicPlayerList(std::vector<publicPlayer*>* newLocalPlayerList);

    //Q_Properties
    /**
     * @brief getIpText returns the ip entered in the join game form
     * @return the ip
     */
    QString getIpText() const;

    /**
     * @brief setIpText sets the ip to join to
     * @param ipText
     */
    void setIpText(const QString &ipText);

    /**
     * @brief getPlayerHandCards returns the QVector containing all the cards the player has on its hand
     * @return vector of abstract cards
     */
    QVector<AbstractCard::CardTypes> getPlayerHandCards();

    /**
     * @brief getAmountOfCardsInDrawPile
     * @return
     */
    unsigned int getAmountOfCardsInDrawPile() const;

    /**
     * @brief setAmountOfCardsInDrawPile sets the value of the drawpile on change
     * @param amountOfCardsInDrawPile the new value
     */
    void setAmountOfCardsInDrawPile(unsigned int amountOfCardsInDrawPile);


    /**
     * @brief getAmountCards helper method for QPropertys of all the cardTypes. Is used to get the specific amount of all cards on the playersHand
     * @param type is to determine the card type
     * @return the amount of cards in the players hand (between 0 and n)
     */
    int getAmountCards(AbstractCard::CardTypes type);

    //these are all the specific methods using the method above
    /**
     * @brief getAmountAttackCards used in getAmountCards to get the cards of type Attack
     * @return int value (for enum)
     */
    int getAmountAttackCards();

    /**
     * @brief getAmountDefuseCards  used in getAmountCards to get the cards of type Defuse
     * @return int value (for enum)
     */
    int getAmountDefuseCards();

    /**
     * @brief getAmountExplosiveKittenCards used in getAmountCards to get the cards of type Kitten
     * @return int value (for enum)
     */
    int getAmountExplosiveKittenCards();

    /**
     * @brief getAmountFavorCards used in getAmountCards to get the cards of type Favor
     * @return int value (for enum)
     */
    int getAmountFavorCards();

    /**
     * @brief getAmountNopeCards used in getAmountCards to get the cards of type Nope
     * @return int value (for enum)
     */
    int getAmountNopeCards();

    /**
     * @brief getAmountSeeTheFutureCards used in getAmountCards to get the cards of type See the future
     * @return int value (for enum)
     */
    int getAmountSeeTheFutureCards();

    /**
     * @brief getAmountShuffleCards used in getAmountCards to get the cards of type Shuffle
     * @return int value (for enum)
     */
    int getAmountShuffleCards();

    /**
     * @brief getAmountSkipCards used in getAmountCards to get the cards of type Skip
     * @return int value (for enum)
     */
    int getAmountSkipCards();

    /**
     * @brief getAmountBikiniCatCards used in getAmountCards to get the cards of type Bikini Cat
     * @return int value (for enum)
     */
    int getAmountBikiniCatCards();

    /**
     * @brief getAmountCatsSchroedingerCards used in getAmountCards to get the cards of type Cats Schroedinger
     * @return int value (for enum)
     */
    int getAmountCatsSchroedingerCards();

    /**
     * @brief getAmountMommaCatCards used in getAmountCards to get the cards of type Momma Cats
     * @return int value (for enum)
     */
    int getAmountMommaCatCards();

    /**
     * @brief getAmountShyBladderCards used in getAmountCards to get the cards of type Shy Bladder
     * @return int value (for enum)
     */
    int getAmountShyBladderCards();

    /**
     * @brief getAmountZombieCatCards used in getAmountCards to get the cards of type Zombie Cat
     * @return int value (for enum)
     */
    int getAmountZombieCatCards();


private slots:

    /**
     * @brief gameHasEnded is called whe the host game is finished
     */
    void gameHasEnded();   //conneted from GP

    /**
     * @brief refreshGUIHandCards helper method for refreshing the GUI
     */
    void refreshGUIHandCards();
};



#endif // GAMEADAPTER_H

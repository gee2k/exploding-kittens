import QtQuick 2.0

Item {
    id: player_item
    width: parent.width
    height: parent.height * 0.166
    property alias playername : playername
    property alias cardcount : cardcount
    property alias color : wrapper.color
    property var ip: ;

    Rectangle {
        id: wrapper
        color: "#99ffffff";
        anchors.fill: parent
    }

    Text {
        id: playername
        y: 0
        width: parent.width * 0.66
        height: parent.height
        color: "#ededed"
        text: qsTr("PLAYER") + gameAdapter.translation
        anchors.left: parent.left
        anchors.leftMargin: 10
        font.pixelSize: 22
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft

        Rectangle {
            id: seperator
            width: 2
            height: parent.height * 0.8
            color: "#ededed"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.right
            anchors.leftMargin: -10
        }
    }

    Rectangle {
        id: cardcountwrapper
        x: 0
        y: 0
        width: parent.width * 0.34
        height: parent.height
        color: "#00000000"
        //radius: 20
        anchors.right: parent.right
        anchors.rightMargin: 0
        //border.width: 6

        Text {
            id: cardcount
            color: "#ededed"
            text: "0"
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 22
            font.bold: true
        }
    }


    /*
        \qmlmethod setName(name)

        This method sets the name label text to the name in parameter
    */
    function setName(name) {
        playername.text = name;
    }

    /*
        \qmlmethod setCardCount(count)

        This method sets the shown amount of cards in the player item to that given in parameter
    */
    function setCardCount(count) {
        cardcount.text = count;
    }

    /*
        \qmlmethod destroyThis()

        This method destroys this item
    */
    function destroyThis() {
        player_item.destroy();

        console.log(gameform.playerComponents.length);
    }

}

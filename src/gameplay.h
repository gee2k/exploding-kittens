#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include <QObject>
#include <vector>
#include "player.h"
#include "stateMachine.h"
#include "deck.h"
#include <QTime>
#include <thread>
#include "client.h"
#include "server.h"
#include "cards.h"
#include "gameplaysettings.h"


/**
 * @brief The gamePlay class is the center object which runs the game including its state transitions and communication
 */
class gamePlay : public QObject{

    Q_OBJECT

public:

    /**
     * @brief gamePlay constructor
     * @param playerlist the attenting players in the game
     */
    gamePlay(std::vector<privatePlayer*>* playerlist);


protected:

    /**
     * @brief attendingPlayer holds the players in the game
     */
    std::vector<privatePlayer*>* attendingPlayer;       //gets deleted in adapter

    /**
     * @brief amountPlaying the amount of players
     */
    int amountPlaying;

};

/**
 * @brief The hostGameplay class is the specailisation for the gameplay running on the host
 */
class hostGameplay : public gamePlay{

    Q_OBJECT

public:
    using gamePlay::gamePlay;
    ~hostGameplay();

    /**
     * @brief start starts the game by creating all the states, connections and the timer.
     */
    void start();

    /**
     * @brief assignServer puts the network object to the gameplay to access it
     * @param passedServer the server used for network
     */
    void assignServer(Server* passedServer);

signals:
    /**
     * @brief gameEndedSetNewGameReady is emitted to the gameAdapter when a game finished
     */
    void gameEndedSetNewGameReady();    //connected to GA

    /**
     * @brief deckCreatedSendToPlayer emittes the cards for a specific ip
     * @param ip of the player
     * @param cards for the player
     */
    void deckCreatedSendToPlayer(QString ip, std::vector<AbstractCard::CardTypes> cards);   //connected to server

    /**
     * @brief sig_gamePaused is emitted when the game is on hold
     */
    void sig_gamePaused();

    /**
     * @brief sig_gameResumed is emitted when the game resumes from hold
     */
    void sig_gameResumed();

public slots:
    /**
     * @brief slt_gameEnded is used by the StateMachine End GameState, when the game finished
     */
    void slt_gameEnded();    //connected from SM(EGS)

    /**
     * @brief slt_changedState is emitted everytime a state transition happened
     * @param newState into which was transitioned
     */
    void slt_changedState(BaseState::playStates newState); //connected from SM(all)

    /**
     * @brief slt_updateState is the core update routine for the statemachine
     */
    void slt_updateState(); //connected from Timer - for stateMachine run

    //from network
    /**
     * @brief slt_playerLeftGame is called from the adapter when a player left the game while a game was running.
     * the statemachine is paused, moves done in this turn are reverted and the refreshed player list ist emitted to the clients
     * then the game continues
     *
     * it could be differentiated between active player left or not but it could have unforseen consequences when ophan cards are in play
     *
     * @param ip of the player who left
     */
    void slt_playerLeftGame(QString ip);    //connected from GA




private:

    /**
     * @brief stateTracker holds all created states for easier managing (connections and deletions)
     */
    std::vector<BaseState*> *stateTracker;

    /**
     * @brief stateMachine, what do i need to say more
     */
    BaseState* stateMachine;

    /**
     * @brief m_drawPile is the games Drawile which holds the cards for the players
     */
    DrawDeck* m_drawPile;

    /**
     * @brief m_discardPile holds the played cards
     */
    Deck* m_discardPile;

    /**
     * @brief m_playedCards holds the cards in play (per round) bevor they are discarded on the discard pile
     */
    std::vector<PlayedCard*>* m_playedCards;

    /**
     * @brief elapsedTime actual elapsed Time which is taken to comparison with timestamp (greater than some delta to change states)
     */
    int elapsedTime = 0;

    /**
     * @brief timeStamp is reset to actual time after stateSwitch for stateMachine comparison and elapsed time calculation
     */
    int timeStamp = 0;

    /**
     * @brief timer
     */
    QTimer *timer;

    /**
     * @brief server component for client communication
     */
    Server* server;

    /**
     * @brief cardBuffer just the buffer for holding cards before sending them to the clients
     */
    std::vector<AbstractCard::CardTypes>* cardBuffer;

    /**
     * @brief sendBufferToNetwork is used to send the created decks on playstart to the players
     * @param ip of the player getting its deck
     */
    void sendBufferToNetwork(QString ip);

    /**
     * @brief getSystemTime returns the system time in msec
     * @return the actual system time
     */
    int getSystemTime();

    /**
     * @brief removeAllClients called when the host left the game and all clients need to be informed
     */
    void removeAllClients();
};

#endif // GAMEPLAY_H

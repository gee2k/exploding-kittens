#ifndef DECK_H
#define DECK_H
#include <QObject>
#include "cards.h"
#include <QDebug>
#include <stdlib.h>
#include <unordered_map>
#include <time.h>
#include <vector>

/**
 * @brief The Deck class is the base class for all deck types in game. It is holding the information about the cards inside them and adds base funktionaltiy including some for the gamerules
 */
class Deck : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief The DeckType enum holds the different deck types
     */
    enum DeckType{
        DrawPile,
        HandCards,
        emptyDeck
    };
    /**
     * @brief Deck constructor to create a deck
     * @param deckType of which type of deck it should be
     * @param playerAmount is set to 0 when not a drawPile is created
     */
    Deck(DeckType deckType = DeckType::emptyDeck, int playerAmount = 0);

    /**
     * @brief getDeckSize returns the amount of types in the deck
     * @return int amount of cards types
     */
    virtual int getDeckSize() const;

    /**
     * @brief addCard adds a card to the deck
     * @param cardType is the type of card which should be added
     */
    virtual void addCard(AbstractCard::CardTypes cardType); //needs to be public, signals use it for the "cardsPlayedThisRound pile"

    /**
     * @brief addCards adds many cards at once to the deck
     * @param cards a vector containing the cards to add
     */
    virtual void addCards(std::vector<AbstractCard::CardTypes> cards); //needs to be public, signals use it for the "cardsPlayedThisRound pile"

    /**
     * @brief fillDeckWithCards fills the deck depending of the amount of players with the appropriate cards (like the playedsize depending amount of kittens and defuse)
     * @param playerAmount the amount which compete in the game
     */
    virtual void fillDeckWithCards(int playerAmount);

    //test methods
    /**
     * @brief countCards counts the amount of cards in the deck
     * @return int with the amount of cards
     */
    virtual int countCards();

    /**
     * @brief printRemainingCards prints the card type and the amount of it tho the console
     */
    virtual void printRemainingCards();

protected:
    /**
     * @brief m_cards internal storage for the cards
     */
    QMap<AbstractCard::CardTypes,int> m_cards;              //key value list having all cards and their amount inside the deck

    /**
     * @brief m_deckType holds the type of the deck
     */
    DeckType m_deckType;

};

/**
 * @brief The DrawDeck class is the class for all the cards the player decks are derived and the main game takes the cards from
 */
class DrawDeck : public Deck
{
    Q_OBJECT

public:

    /**
     * @brief DrawDeck creates a drawPile
     * @param playerAmount of players who are in the game the drawPile is created for
     */
    DrawDeck(int playerAmount);

    /**
     * @brief addKittenAtPosition adds the kitten card on a position in the draw pile
     * @param kitten is the cardtype AbstractCard::ExlosiveKitten Card
     * @param position is 0 to size of the deck +1
     */
    void addKittenAtPosition(AbstractCard::CardTypes kitten, int position);

    /**
     * @brief shuffleDeck shuffles the deck
     */
    void shuffleDeck();

    /**
     * @brief drawCard returns the top card from the deck after finalization or the Error Card if deck has not been finalized. The Card is removed from the deck
     * @return AbstractCard::CardType card or ErrorCard on Error
     */
    AbstractCard::CardTypes drawCard();             //player draws a card via this

    /**
     * @brief finalizeDeck can be executed when all player decks are made from the draw pile. Then the cards are all shuffled. And the Deck is set to finalized
     */
    void finalizeDeck();

    /**
     * @brief countCards counts the card of the drawDeck. This version is modified from the base class
     * @return the amount of cards in the drawPile
     */
    int countCards();                               //overwrite

    /**
     * @brief printRemainingCards prints the remaining cards of the deck to the console
     */
    void printRemainingCards();                     //overwrite

    /**
     * @brief derivePlayerDeck returns a player Deck from the drawPile.
     * @return a vector with 5 cards (starter deck for player)
     */
    std::vector<AbstractCard::CardTypes> derivePlayerDeck();

    /**
     * @brief adjustDeckBecauseofPlayerLeave is called from the GP when a player has left so that there is one kitten less than players in game
     * @param amountOfPlayer is the new amount
     */
    void adjustDeckBecauseofPlayerLeave(unsigned int amountOfPlayer);

    /**
     * @brief activateSeeTheFuture returns the top 3 cards for view
     * @return 3 cards to view
     */
    std::vector<AbstractCard::CardTypes> activateSeeTheFuture();

    //tests
    /**
     * @brief readFirstCard returns the value of the first card in the draw pile
     * @return CardType of the Card
     */
    AbstractCard::CardTypes readFirstCard() const;

    /**
     * @brief getCardAtPosition returns the card at a specific position in the draw pile
     * @param i is the position
     * @return CardType of the Card at that position
     */
    AbstractCard::CardTypes getCardAtPosition(int i);


private:

    /**
     * @brief m_deck holds the randomized cards for drawing after finalisation
     */
    QList<AbstractCard::CardTypes>* m_deck;

    /**
     * @brief m_decksDerived amount of player Decks derived. If amount of players reached. Deck is being finalized.
     */
    int m_decksDerived = 0;

    /**
     * @brief m_setupFinished till player decks are created and this deck got shuffled. The randomizanion is done so no more decks can be detached.
     */
    bool m_setupFinished = false;

    /**
     * @brief m_playerAmountToCreateDeck like the name says
     */
    int m_playerAmountToCreateDeck;

    /**
     * @brief getRandomNotDefuseNorKittenCard returns a Card which is not of Type Explosive Kitten or Defuse
     * @return CardType which is drawn
     */
    AbstractCard::CardTypes getRandomNotDefuseNorKittenCard();

    /**
     * @brief getRandomCard returns a random card from the deck
     * @return DeckType randomly drawn
     */
    AbstractCard::CardTypes getRandomCard();

    /**
     * @brief getSpecificCard returns the demanded card if available. Else an Error Card
     * @param cardType the card which should be returned
     * @return The card given in the parameters or an error card
     */
    AbstractCard::CardTypes getSpecificCard(AbstractCard::CardTypes cardType);  //for player deck building


signals:
//    void sig_derivedDeck(std::vector<AbstractCard::CardTypes> deck);
};

/**
 * @brief The PlayerDeck class is the deck type a players cards are stored. it holds methods for the gameplay and gamerules
 */
class PlayerDeck : public Deck
{
    Q_OBJECT

public:

    /**
     * @brief PlayerDeck constructor
     */
    PlayerDeck();

    /**
     * @brief PlayerDeck constructor
     * @param refToPlayDeck is the drawPile were the cards are taken from
     */
    PlayerDeck(DrawDeck *refToPlayDeck);

    /**
     * @brief getRandomCard returns a random card from the (player) deck
     * @return CardType draw
     */
    AbstractCard::CardTypes getRandomCard();    

    /**
     * @brief getSpecificCard returns the card given or the error card if the given card is not inside the player deck. the Card is removed from the deck
     * @param cardType which should be returned
     * @return CardType
     */
    AbstractCard::CardTypes getSpecificCard(AbstractCard::CardTypes cardType);  //for player deck building

    /**
     * @brief readCards returns a vector with all cardTypes the player has on its hand
     * @param refDeckForGui which should hold the decks cardTypes
     */
    void readCards(QVector<AbstractCard::CardTypes> &refDeckForGui);

    /**
     * @brief readDeckToSendToNetwork puts the deck cards into the buffer as parameter
     * @param buffer is a vector getting the cards of the deck
     */
    void readDeckToSendToNetwork(std::vector<AbstractCard::CardTypes> *buffer);

    /**
     * @brief getAmountOfCards returns the amount of cards from a card type inside the playerdeck
     * @param card
     * @return int amount of cards
     */
    int getAmountOfCards(AbstractCard::CardTypes card);
private:

    /**
     * @brief m_referenceDeck is the drawPile to take the cards from on creation
     */
    Deck *m_referenceDeck;

    /**
     * @brief sentToNetwork is a safe condition that a deck can only be sent once on creation
     */
    bool sentToNetwork = false;
};

//Q_DECLARE_METATYPE(Deck)

#endif // DECK_H

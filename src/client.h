#ifndef CLIENT_H
#define CLIENT_H

#include "QTcpServer"
#include "QTcpSocket"
#include "QDataStream"
#include "vector"
#include "networkheaders.h"
#include "cards.h"
#include "player.h"
#include <QNetworkInterface>
#include "stateMachine.h"
#include "logging.h"

/**
 * @brief The Client class
 *
 * This class represents the network client which listens to a server
 * and sends messages to it when the player does an action which has
 * to be send to the server
 */
class Client : public QObject
{
    Q_OBJECT
public:

    /**
     * @brief Client is the constuctor of this class
     */
    Client();

    /**
     * @brief ~Client is the destructor of this class
     */
    ~Client();

    /**
     * @brief establishes the connection to a host
     * @param adress the IP address of the host the client wants to connect to
     */
    void establish(QString adress);

    /**
     * @brief the main method for communication between server and client
     *
     * Sends messages to the host and is used by slots which are meant to prepare
     * game data for transport over the network
     * @param buffer the message which has to be sent
     */
    void sendNetworkSignal(QByteArray& buffer);

    /**
     * @brief method to match an IP to own network interfaces
     *
     * This method checks if the given IP matches to one of the client machines addresses
     * @param ip the address to match
     * @return true when IP matches
     */

    /**
     * @brief client socket which is essential for the connection to a host
     */
    QTcpSocket socket;
public slots:

    /**
     * @brief sends the nickname of the player to the server
     */
    void addSelf(QString name);

    /**
     * @brief main method for reading incomming network messages
     *
     * Every network message is read in this method and the right signals
     * are sent based on the message
     */
    void readNetworkSignal();

    /**
     * @brief translates played cards into a network message
     * @param cards the vector of cards which were played by the player
     */
    void playCards(std::vector<AbstractCard::CardTypes> cards); //connected from GA

    /**
     * @brief sends a disconnect message to the server
     *
     * Sends a disconnect message when the player decides to quit the game
     */
    void sendDisconnect();

    /**
     * @brief slt_connected is used when a connect signal is emitted
     */
    void slt_connected();

    /**
     * @brief slt_disconnected  is used when a disconnect signal is emitted
     */
    void slt_disconnected();

    /**
     * @brief slt_sendServerKittenPositionInDrawPile sends the position of the kitten to the server
     * @param position inside the drawpile
     */
    void slt_sendServerKittenPositionInDrawPile(int position);


signals:

    /**
     * @brief emitted when server announces the current player
     * @param ip the current players ip address
     */
    void activePlayerReceived(QString ip);

    /**
     * @brief emitted when cards are received to be shown in the gui
     * @param ip the player ip address
     * @param cards cards which were played
     */
    void playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards);

    /**
     * @brief emitted when a game is started by the host
     */
    void gameIsStarting();  //connected to GA

    /**
     * @brief emitted when a player quits the game
     * @param ip the quitting players ip address
     */
    void playerDisconnected(QString ip);    //connect to GA

    /**
     * @brief emitted when the host announces a winner
     * @param ip ip address of the winner
     */
    void playerWon(QString ip);

    /**
     * @brief emitted every time a player explodes
     * @param ip is the ip address of the looser
     */
    void playerLost(QString ip);

    /**
     * @brief emitted when a new playerlist is received from the host
     * @param players is the new player list
     */
    void sig_receivedNewPlayerlist(std::vector<publicPlayer*>* players);    //connected to GA

    /**
     * @brief connected is emitted when the client has estabilished a connection
     */
    void sig_connected();   //connected to GA

    /**
     * @brief disconnected is emitted when the connection is lost
     */
    void sig_disconnected();    //connected to GA

    /**
     * @brief sig_receivedSeeTheFutureCards emitted when the top 3 drawPile cards arrive at the client
     * @param cards containing the peek cards
     */
    void sig_receivedSeeTheFutureCards(std::vector<AbstractCard::CardTypes> cards);

    /**
     * @brief sig_newStateReceived is emitted when a new state is received
     * @param state the received state
     */
    void sig_newStateReceived(BaseState::playStates state);

    /**
     * @brief sig_receivedCards is emitted when cards are received
     * @param cards the received cards
     */
    void sig_receivedCards(std::vector<AbstractCard::CardTypes> cards);

    /**
     * @brief sig_receivedCard is emitted when a card is received
     * @param card the card which was received
     */
    void sig_receivedCard(AbstractCard::CardTypes card);

    /**
     * @brief sig_gameEnded is emitted when the game has ended
     */
    void sig_gameEnded();

    /**
     * @brief sig_receivedCardAmount is emitted when the cardamount of a player is received
     * @param ip the players ip
     * @param amount the card amount of the players hand deck
     */
    void sig_receivedCardAmount(QString ip, uint amount);   //connected to GA

    /**
     * @brief sig_shuffleReceived is emitted when the deck was shuffled
     */
    void sig_shuffleReceived(); //connected to GA

    /**
     * @brief sig_receivedDrawPileCount is emitted when the cound of cards in drawpile is received
     * @param amount the amount of cards
     */
    void sig_receivedDrawPileCount(uint amount);

    /**
     * @brief sig_errorMessage is emitted when an error message is received
     * @param msg the content of the message
     */
    void sig_errorMessage(QString msg);

    /**
     * @brief ipReceived is emitted when the own ip is received from the server
     * @param ip the ip address
     */
    void ipReceived(QString ip);

    /**
     * @brief sig_receivedPlayerStats is emitted when the player statistics are received from server
     * @param playerStatistics the statistics
     * @param playerList the list of players you played against
     */
    void sig_receivedPlayerStats(QMap<int, unsigned int> playerStatistics, QString playerList);

    /**
     * @brief sig_receivedGameStats is emitted when the game statistics are received
     * @param gameBestOfStatistics the game statistics
     */
    void sig_receivedGameStats(QMap<int, QMap<QString, unsigned int> >gameBestOfStatistics);

    /**
     * @brief sig_progressChangedReceived is emitted when the progress of the current state is received
     * @param percent the progress in percent from 0 to 1
     */
    void sig_progressChangedReceived(double percent);

    /**
     * @brief sig_receivedRemoveCard is emitted when a card which has to be removed from player deck is received
     * @param card the card to be removed
     */
    void sig_receivedRemoveCard(AbstractCard::CardTypes card);
private:

    /**
     * @brief the communication port
     */
    quint16 port = 1337; //LEEEEEEET
};

#endif // CLIENT_H

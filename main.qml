import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Particles 2.0

Window {
    property alias root: root
    id: root
    /*width: 1280
    height: 720*/
    width: Screen.desktopAvailableWidth
    height: Screen.desktopAvailableHeight
    minimumWidth: 800
    minimumHeight: 600
    x: 0
    y: 0
    visible: true
    visibility: Window.Windowed

    MainMenu{
        anchors.fill: parent
    }
}

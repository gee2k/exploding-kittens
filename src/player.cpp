#include "player.h"

publicPlayer::publicPlayer(QString playerName, QString playerIPAdress):m_playerName(playerName), m_playerIPAdress(playerIPAdress){}

QString publicPlayer::getPlayerName() const{
    return m_playerName;
}

void publicPlayer::setPlayerName(const QString &name){
    if (name != m_playerName) {
                m_playerName = name;
                emit playerNameChanged();
                qDebug() << "Playername changed to : " << m_playerName;
    }
}



bool publicPlayer::isReady() const{
    return m_isReady;
}

bool publicPlayer::getPlayerLost() const{
    return hasLost;
}

int publicPlayer::getAmountOfCardsOnHand() const
{
    return amountOfCardsOnHand;
}

void publicPlayer::setAmountOfCardsOnHand(int value)
{
    amountOfCardsOnHand = value;
}


QString publicPlayer::getIPAddress() const
{
    return m_playerIPAdress;
}

void publicPlayer::setIPAddress(QString ipAdress)
{
    m_playerIPAdress = ipAdress;
}


bool publicPlayer::operator==(const publicPlayer* p)
{
    qDebug() << "comparing " << this->getPlayerName() << " == " << p->getPlayerName();
    qDebug() << "comparing " << this->getIPAddress() << " == " << p->getIPAddress();
    return (this->getPlayerName() == p->getPlayerName() && this->getIPAddress() == p->getIPAddress());
}


bool publicPlayer::operator!=(const publicPlayer* p)
{
    return !(this == p);
}

//private player from here

privatePlayer::~privatePlayer()
{
    delete playerDeck;
}

void privatePlayer::setPlayerReady(){
    m_isReady = !m_isReady;
}

void privatePlayer::assignDeck(PlayerDeck *deck){
//    playerDeck != NULL ? true: false;
    playerDeck = deck;
}

void privatePlayer::setPlayerLost(){
    hasLost = true;
}

void privatePlayer::logPlayedCard(AbstractCard::CardTypes card)
{
    playerLogging.playedCard(card);
}

void privatePlayer::setLoggingNopingToFinished()
{
    playerLogging.finishedNopingRound();
}

QMap<Logging::logVals, unsigned int> privatePlayer::getReport() const
{
    return playerLogging.getReport();
}




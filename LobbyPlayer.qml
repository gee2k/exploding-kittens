import QtQuick 2.0

Item {
    id: item1
    width: parent.width
    height: parent.height * 0.2
    property alias playername : playername
    property alias btn: kick_btn_wrapper
    property var ip: ;

    signal kick(string ip);

    Text {
        id: playername
        y: 0
        width: parent.width * 0.66
        height: parent.height
        text: qsTr("PLAYER") + gameAdapter.translation
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 10
        font.pixelSize: 22
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
    }

    Rectangle {
        id: kick_btn_wrapper
        height: parent.height
        color: kick_btn_mouseArea.containsMouse ? "#99000000" : "#99d14141"
        anchors.left: playername.right
        anchors.leftMargin: 0
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 0

        Text {
            id: kick_btn_text
            color: kick_btn_mouseArea.containsMouse ? "#ededed" : "#d31111"
            text: "X"
            font.bold: true
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 32
        }

        MouseArea {
            id: kick_btn_mouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                gameAdapter.slt_kickPlayer(ip);
                console.log("kick called");
            }
        }
    }

    /*
        \qmlmethod setName(name)

        This method sets the name text label to the name given in parameter
    */
    function setName(name) {
        playername.text = name;
    }

    /*
        \qmlmethod destroyThis()

        This method destroys the lobby player item
    */
    function destroyThis() {
        item1.destroy();
        console.log(hostGameForm1.players.toString())
    }
}

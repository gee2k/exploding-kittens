#include "stateMachine.h"



//baseState
//int BaseState::getActivePlayer() const
//{
//    qDebug() << "BaseState: reading active player: " << activePlayer;
//    return activePlayer;
//}

//void BaseState::setActivePlayer(const int &player)
//{
//    qDebug() << "BaseState: setting active player to: " << player;
//    activePlayer = player;
//}

bool BaseState::checkCards(std::vector<AbstractCard::CardTypes> &cards)
{
    //check the cards in the players deck
//    AbstractCard::CardTypes cardToActivate = (playedCards->at(0)->getCardType());
    AbstractCard::CardTypes cardToActivate = (cards.at(0));

    bool check = false;

    //1 card -> direct compare
    if (amountOfPlayedCards == 1){
        qDebug() << "[CARD SEC CHECK] check for 1 card";
        int amountCheck = attendingPlayer->at(activePlayer)->playerDeck->getAmountOfCards(cardToActivate);
        check = (amountCheck > 0);
        qDebug() << "[CARD SEC CHECK] check of 1 card is " << check << " ,amount in deck is: " << amountCheck;
    }
    else
    {
        //more than 1 card must all be the same (no spcial combos support today :()
        PlayerDeck compareDeck;

        //fill comparison deck with played cards and compare them
        for (int i = 0; i < amountOfPlayedCards; ++i){

            //first fill the comparison deck ...
//            compareDeck.addCard(playedCards->at(i)->getCardType());
            compareDeck.addCard(cards.at(i));


            // check that all are the same -> after adding the cards the amount if the first card should be the amount of loops
//            check = compareDeck.getAmountOfCards(playedCards->at(i)->getCardType()) == i;
            check = compareDeck.getAmountOfCards(cards.at(i)) == i;


            if (!check){ //card amount error -> break

                qDebug() << "[CARD SEC CHECK] - Error in card amount check -> ABORT CARD ACTIVATION !";
                break;
            }
        }

        //...then compare amount of cards in the hand cards of the player
        if (check)
            check = (compareDeck.getAmountOfCards(cardToActivate) >= attendingPlayer->at(activePlayer)->playerDeck->getAmountOfCards(cardToActivate));
    }

    return check;
}

BaseState::BaseState(BaseState* next_a, BaseState* next_b, playStates name, int duration, std::vector<privatePlayer *> *playerlist, DrawDeck *drawPile, std::vector<PlayedCard*> *playedCards, Deck *discardPile): nextState_A(next_a), nextState_B(next_b), name(name), duration(duration), attendingPlayer(playerlist), nextState(nextState_A), drawPile(drawPile), playedCards(playedCards), discardPile(discardPile){
    signalMapper = new QSignalMapper(this);
    qDebug() << "Created State: " << this->getName() << " with duration: " << duration;
}

BaseState::BaseState(BaseState* next_a, BaseState* next_b, playStates name, int duration): nextState_A(next_a), nextState_B(next_b), name(name), duration(duration), nextState(nextState_A){
    signalMapper = new QSignalMapper(this);
    qDebug() << "Created State: " << this->getName() << " with duration: " << duration;
}

BaseState::BaseState():nextState_A(NULL), name(BaseState::stateMachine), duration(0){
    signalMapper = new QSignalMapper(this);
    qDebug() << "Created State: " << this->getName() << " with duration: " << duration;
}

BaseState::~BaseState() { }    //inline um die "One Definition Rule" nicht zu verletzen (was auch immer das heißt)

void BaseState::setNextState(BaseState* next){
    nextState = next;
}

void BaseState::setNextState_A(BaseState* next_A){
    nextState_A = next_A;   //dynamic_cast<subState*>(nextState);
    nextState = next_A;
}

void BaseState::setNextState_B(BaseState* next_B){
    nextState_B = next_B;   //dynamic_cast<subState*>(nextState);
}

BaseState::playStates BaseState::getName() const{
    return name;
}

BaseState* BaseState::getNextState() const{
    return nextState;
}

BaseState* BaseState::getNextState_A() const{
    return nextState_A;
}

BaseState* BaseState::getNextState_B() const{
    return nextState_B;
}



BaseState* BaseState::update(int &timeStamp, int refTime){

    if (!this->stateActive)
        this->stateActive = true;

    //debug
//        qDebug() << "inside State: " << QString::fromStdString(getName());
//        qDebug() << (timeStamp + duration) << " > " << refTime;

//        qDebug() << "Check goes: " << QString::number(timeStamp + duration) << " > " << QString::number(refTime);
//    while((timestamp + duration) > currentTime)                 //compare Time

    //emit time left in state
    double remainingTime = (static_cast<double>(((timeStamp + duration) - refTime)*(duration))) / 100;
//    qDebug() << "[TIMER] " << (remainingTime);   //10 offset
//    qDebug() << "[TIMER] " << remainingTime;
    emit sig_refreshTimer(remainingTime);

    //if((timeStamp + duration) > currentTime)                 //compare Time
    if(((timeStamp + duration) < refTime) || (setStateToSwitchState == true))//compare Time
    {
        qDebug() << "[SM] switch state to: " << nextState->getName();
        emit sig_changedState(nextState->getName());

        //reset setStateToSwitchState for next run of state
        setStateToSwitchState = false;

        //deactivate current state
        stateActive = false;

        //run cleanup
        stateCleanUp();

        //switch state
        return nextState;
    }
    else
    {
        //process Logic here
        gameLogic();

        return this;
    }
}

void BaseState::setGameRestartedBecauseOfLeave(unsigned int playerPosLeft)
{
    m_gameResumed = true;
    m_positionOfLeaver = playerPosLeft;
}

//void BaseState::assignDrawPile(DrawDeck *drawDeck)
//{
//    drawPile = drawDeck;
//}

//void BaseState::assignAttentingPlayer(std::vector<privatePlayer *> *players)
//{
//    attendingPlayer = players;
//}

int BaseState::activePlayer = -1;
int BaseState::amountOfPlayedCards = 0;
bool BaseState::thisPlayerExtraRound = false;
bool BaseState::nextPlayerExtraRound = false;

//gameStates from here

/*
 * Switch player state
 * changes to the next valid player in attending players list
 * sends it over the network to all attending players
 *
 * */
void SwitchPlayerState::gameLogic(){
    qDebug() << "[SM SWS] processing logic inside " << this->name;

    //check if the game has been resumed
    if(!m_gameResumed){

        //if activePlayer = -1 then this game just began
        //pick random from 0 to attendingSize - 1 for first player
        if (activePlayer < 0){

            //pick random player
            qDebug() << "[SM SWS] pick a random player";
            activePlayer = rand() % attendingPlayer->size();

            //            qDebug() << "[SM - SWP] active Player: " << activePlayer;
            qDebug() << "[SM SWS] Player: " << attendingPlayer->at(activePlayer)->getPlayerName() << " begins";
            playerSet = true;
        }
        else if (!thisPlayerExtraRound){
            //check if the player has not lost
            //            qDebug() << "[SM - SWP] switching to next player";
            do{
                activePlayer = ((++activePlayer) % attendingPlayer->size());
                //            qDebug() << "try next active player #" << activePlayer+1 << " of " << attendingPlayer->size() << " lost ? " << attendingPlayer->at(activePlayer)->getPlayerLost();
                if(!attendingPlayer->at(activePlayer)->getPlayerLost())
                    break;
            }
            while (attendingPlayer->at(activePlayer)->getPlayerLost());

            //set extraround if last player played an attack card
            if(nextPlayerExtraRound){
                qDebug() << "[SM SPS] Attack was played before, enjoy your extra turn";
                thisPlayerExtraRound = true;
                nextPlayerExtraRound = false;
            }

            qDebug() << "[SM SWS] next player is: " << attendingPlayer->at(activePlayer)->getPlayerName() << " with ip " << attendingPlayer->at(activePlayer)->getIPAddress();
        }
        else{
            qDebug() << "[SM SPS] player has an extra Turn";
            thisPlayerExtraRound = false;
        }


        //clear the playedCards buffer and add them to the discardPile
        qDebug() << "[SM SPS] clearing played Cards Buffer";
        if (playedCards->size() > 0){
            for (auto iter = playedCards->begin(); iter!= playedCards->end();){
                discardPile->addCard((*iter)->getCardType());
                iter = playedCards->erase(iter);
                qDebug() << "[SM SPS] clearing size: " << playedCards->size();
            }
        }


    }
    else{
        //game has been resumed so one player less in the attenting player list

        //if leaver pos is < then actualPlayerPos -> playerPos -1
        if (m_positionOfLeaver < activePlayer)
            --activePlayer;
        //if behind actual no changes needed, active player gets his turn

        //if the active player left, them the next switched to its position


        //check that not the last in list left and was active so no out of bound error occures
        if (activePlayer == attendingPlayer->size())
            activePlayer = 0;

        //m_gameResumed deactivate
        m_gameResumed = false;
    }

    emit sigSendActivePlayer(attendingPlayer->at(activePlayer)->getIPAddress());

    // set state to switch to next state
    setStateToSwitchState = true;
}

void SwitchPlayerState::stateCleanUp(){}

SwitchPlayerState::~SwitchPlayerState(){}


/*
 * GameLogic for playcard state
 * state waits if there was a card played
 * checks if the cards comes from the valid player
 * and switches the state
 *
 * */
void PlayCardState::gameLogic(){
    //nothing fancy here, waits for the network signal in the slot below
    //    qDebug() << "[SM - PCS] processing logic inside " << QString::fromStdString(this->name);

    //reset State to standart Transition
    if (nextState == nextState_B)
        nextState = nextState_A;
}

void PlayCardState::stateCleanUp(){}

void PlayCardState::slt_playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards){

    if (stateActive){

        bool check = false;
        qDebug() << "[SM PCS] - played card received from " << ip;
        //Card played received from network

        //check if player is player which turn it is
        if (ip == attendingPlayer->at(activePlayer)->getIPAddress()){
            //ip adresses matches

            //if cards is 0 -> player doest want to play a card
            if (cards.size() < 1){
                //standart nextstate should be the drawCardState
                setStateToSwitchState = true;
            }
            else{
                //player wants to play cards

                //count cards for later examination
                amountOfPlayedCards = cards.size();

                qDebug() << "SEC CHECK = " << checkCards(cards);

                //add cards to playedCards stack and log them
                for (auto iter = cards.begin(); iter != cards.end(); ++iter){

                    //first check that the cards are on the players deck on the server side (here)
                    check = (*iter == attendingPlayer->at(activePlayer)->playerDeck->getSpecificCard(*iter));
                    qDebug() << "remove card: " << *iter << " from player deck check: " << check;
                    if(!check)
                        attendingPlayer->at(activePlayer)->playerDeck->printRemainingCards();

                    if (check){
                        //add card to buffer
                        //playedCards->addCard(*iter);  //old version
                        qDebug() << "[SM PCS] processing card " << *iter;
                        playedCards->push_back(new PlayedCard(*iter, ip));
                        qDebug() << "[SM PCS] added " << playedCards->back()->getCardType() << " to playedCards (" << playedCards->back() << ")";

                        //add card to player log
                        attendingPlayer->at(activePlayer)->logPlayedCard(*iter);
                    }else{
                        qDebug() << "check failed, card not in serverside player deck";
                        break;
                    }
                }

                //check successful ? -> inform other player
                if(check){

                    //send all players the played cards
                    qDebug() << "[SM PCS] send clients the played cards";
                    emit sig_playerPlayedCard(ip, cards);   //this goes to the network so all players know the cards and their UI can update the view

                    //send the new card amount to the other players GUI
                    qDebug() << "[SM PCS] emitting new amount for " << ip << " with " << attendingPlayer->at(activePlayer)->playerDeck->countCards();
                    emit sig_amountOfCardsChanged(ip, attendingPlayer->at(activePlayer)->playerDeck->countCards());

                    //cards where played, so set state to gatherNopeState
                    nextState = nextState_B;

                    // .. overwrite the timer
                    setStateToSwitchState = true;
                }
                else{
                    qDebug() << "check failed, TODO send cards back";
                }
            }
        }
    }
}

PlayCardState::~PlayCardState() {}

/*
 * Gather Nope State
 * When there is a card played, other players can "nope" that.
 * this is only possible in a timeframe. Nopes can be noped again.
 * a small timer runs after a nope waiting for another one.
 *
 * */

void GatherNopeState::gameLogic(){
    //reset State to standart Transition
    if ((nextState == nextState_B) && (!nopingStarted))
        nextState = nextState_A;
}

void GatherNopeState::stateCleanUp()
{
    qDebug() << "[SM GNS] Cleaning up";
    noped = false;
    nopingStarted = false;
    //deaktivate noping round in logging to not calculate more bonus
    for (auto iter = attendingPlayer->begin(); iter != attendingPlayer->end(); ++iter){
        (*iter)->setLoggingNopingToFinished();
    }
}

void GatherNopeState::slt_playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards){   

    if (stateActive){
        //Card played received from network
        qDebug() << "[SM GNS] - played card received from " << ip;
        bool check = false;

        //check if card is "nope"
        if(cards.size() == 1 && cards.at(0) == AbstractCard::Nope){
            //check ip from "noper" and if he has that card on its stack
//            qDebug() << "SEC CHECK = " << checkCards(cards);  //checks on the active player not the noper...

            //get player with noper ip
            //            qDebug() << "[SM - GNS] - check noper ip";
            for (auto iter = attendingPlayer->begin(); iter != attendingPlayer->end(); ++iter){

                //if ip found
                if ((*iter)->getIPAddress() == ip){


                    //first check that the cards are on the players deck on the server side (here)
                    check = ((*iter)->playerDeck->getSpecificCard(AbstractCard::Nope) == AbstractCard::Nope);
                    qDebug() << "[SM GNS] remove card: " << AbstractCard::Nope << " from player deck check: " << check;
                    if(!check)
                        (*iter)->playerDeck->printRemainingCards();


                    //                    qDebug() << "[SM - GNS] - check for card on players hand";
                    //check if card is in players hand (it will be removed on chck)
                    if(check){

                        nopingStarted = true;

                        //increase noped
                        noped = !noped;
                        qDebug() << "[SM GNS] noped: " << noped;

                        //add cards to playedCards stack
                        //playedCards->addCard(AbstractCard::Nope); //old version
                        playedCards->push_back(new PlayedCard(AbstractCard::Nope, ip));
                        qDebug() << "[SM GNS] added " << playedCards->back()->getCardType() << " to playedCards (" << playedCards->back() << ")";

                        //send nope to all clients
                        qDebug() << "[SM GNS] send clients the played cards";
                        emit sig_playerPlayedCard(ip, cards);

                        //refresh card amount of "noper"
                        //send the new card amount to the other players GUI
                        qDebug() << "[SM GNS] emitting new amount for " << ip << " with " << (*iter)->playerDeck->countCards();
                        emit sig_amountOfCardsChanged(ip, (*iter)->playerDeck->countCards());

                        //add card to player log
                        (*iter)->logPlayedCard(AbstractCard::Nope);

                        //set nextState to noped
                        if (!noped){
                            nextState = nextState_A;    //state if not noped    -> activate_card_State
                        }
                        else{
                            nextState = nextState_B;    //state if noped        -> playcard_state
                        }
                    }
                    else
                        qDebug() << "[SM GNS] Player does not have nope card";

                    break;  //leave loop, player was found
                }
            }
        }
    }
}

GatherNopeState::~GatherNopeState() {}


/*
 * Activate Card State
 * in this state, the played cards are activated
 * main game rules are applied here
 *
 * */
void ActivateCardState::gameLogic(){

    //resetTo standart Transition
    setNextState(nextState_A);


    qDebug() << "[SM ACS] processing logic inside " << this->getName();
    qDebug() << "[SM ACS] there where " << playedCards->size() << " cards played that round";
    qDebug() << "[SM ACS] " << amountOfPlayedCards << " cards to activate";

    AbstractCard::CardTypes cardToActivate = (playedCards->at(0)->getCardType());
    qDebug() << "[SM ACS] cardToCheck: " << cardToActivate << "(" << playedCards->at(0) << ")";

    //check amount played
    if(amountOfPlayedCards == 1){   //only one card played -> action card

        if(cardToActivate == AbstractCard::Shuffle){    //shuffle drawpile
            qDebug() << "[SM ACS] - activating shuffle";
            drawPile->shuffleDeck();
            emit sig_shuffleDeck();
        }

        if(cardToActivate == AbstractCard::Attack){     //set skipped and extraRound for next player
            qDebug() << "[SM ACS] - activating attack";
            nextPlayerExtraRound = true;
            thisPlayerExtraRound = false;
            setNextState(nextState_B);  //switch player state
        }

        if(cardToActivate == AbstractCard::SeeTheFuture){   //send top 3 cards to player
            qDebug() << "[SM ACS] - activating see the future";
            emit sig_sendSeeTheFutureCards(attendingPlayer->at(activePlayer)->getIPAddress(), drawPile->activateSeeTheFuture());
        }

        if(cardToActivate == AbstractCard::Skip){ //set skipped for local player
            qDebug() << "[SM ACS] - activating skip";
            setNextState(nextState_B);  //switch player state
        }

        if(cardToActivate == AbstractCard::Favor){ //named player needs to release a card
            qDebug() << "[SM ACS] - activating favor - easy mode, just random picking...";
            drawRandomCard();
        }
    }
    else if (amountOfPlayedCards == 2){ //2 cards -> pick a random card
        qDebug() << "[SM ACS] - activating pick random card (2 cards played) - easy mode, just random picking...";
        drawRandomCard();
    }
    else if(amountOfPlayedCards == 3){  //3 cards -> call for a special card
        qDebug() << "[SM ACS] - activating call for a specific card (3 cards played) - easy mode, just random picking...";
        drawRandomCard();
    }

    //aaaand switch state
    setStateToSwitchState = true;

    //remember refresh of card deck amount of players
}

void ActivateCardState::stateCleanUp(){}

ActivateCardState::~ActivateCardState(){}

void ActivateCardState::drawRandomCard()
{
    //1. pick a random player not being the issuer of the card
    int randomPerson = 0;
    do{
        randomPerson = rand() % attendingPlayer->size();
        qDebug() << "[SM ACS] pick a random player not beeing the active player (" << attendingPlayer->at(activePlayer)->getIPAddress() <<"): " << randomPerson << " - " << attendingPlayer->at(randomPerson)->getIPAddress();
    }
    while (randomPerson == activePlayer && attendingPlayer->size() > 1);    //size check just for testing when only 1 player plays alone

    //2. return a card to the player from the players hand
    AbstractCard::CardTypes drawnCard = attendingPlayer->at(randomPerson)->playerDeck->getRandomCard(); //removes it from the player deck

    qDebug() << "[SM ACS] Player: " << attendingPlayer->at(activePlayer)->getPlayerName() << " draws: " << drawnCard << " from: " << attendingPlayer->at(randomPerson)->getPlayerName();

    //add card to player deck
    attendingPlayer->at(activePlayer)->playerDeck->addCard(drawnCard);

    //send it to the active player
    QString ip = attendingPlayer->at(activePlayer)->getIPAddress();

    std::vector<AbstractCard::CardTypes> blaVector;
    blaVector.push_back(drawnCard);
    emit sig_playerDrawCard(attendingPlayer->at(activePlayer)->getIPAddress(), blaVector);

    //and tell the client who lost a card which one
    emit sig_playerLostCard(attendingPlayer->at(randomPerson)->getIPAddress(), blaVector);

    //send signal to all other clients that the amount changed
    qDebug() << "[SM ACS] emitting new amount for " << ip << " with " << attendingPlayer->at(activePlayer)->playerDeck->countCards();
    emit sig_amountOfCardsChanged(attendingPlayer->at(activePlayer)->getIPAddress(), attendingPlayer->at(activePlayer)->playerDeck->countCards());
    qDebug() << "[SM ACS] emitting new amount for " << attendingPlayer->at(randomPerson)->getIPAddress() << " with " << attendingPlayer->at(randomPerson)->playerDeck->countCards();
    emit sig_amountOfCardsChanged(attendingPlayer->at(randomPerson)->getIPAddress(), attendingPlayer->at(randomPerson)->playerDeck->countCards());
}

void ActivateCardState::setActivePlayer(int player)
{
    activePlayer = player;
}


/*
 * Draw Card State
 * this state picks a random card from the draw pile and sends it to the player
 * if the player did draw a kitten the game goes into defuse state
 *
 * */
void DrawCardState::gameLogic(){
    qDebug() << "[SM DrawCS] processing logic inside " << this->name;

    //set standart nextState
    nextState = nextState_A;    //should be SwitchPlayer State

    //int activePlayer = BaseState::getActivePlayer();

    if (!skipped){
        //DEBUG
        //        qDebug() << drawPile->countCards() << " cards in the draw Pile";

        //pick a card from the draw pile
        AbstractCard::CardTypes drawnCard = drawPile->drawCard();
        //        qDebug() << "[SM - DCS] drawn Card is: " << static_cast<AbstractCard::CardTypes>(drawnCard);

        //        qDebug() << "[SM - DCS] Active Player Number is: " << activePlayer;
        //        qDebug() << "[SM - DCS] attending players: " << attendingPlayer->size();

        qDebug() << "[SM - DrawCS] Player: " << attendingPlayer->at(activePlayer)->getPlayerName() << " draws: " << drawnCard;

        //add card to player deck
        attendingPlayer->at(activePlayer)->playerDeck->addCard(drawnCard);

        //send it to the active player
        QString ip = attendingPlayer->at(activePlayer)->getIPAddress();

        //cause of the architecture we need a vector... does it stay over the livetime ?
        std::vector<AbstractCard::CardTypes> blaVector;
        blaVector.push_back(drawnCard);
        emit sig_playerDrawCard(ip, blaVector);

        //send signal to all other clients that the amount changed
        qDebug() << "[SM - DrawCS] emitting new amount for " << ip << " with " << attendingPlayer->at(activePlayer)->playerDeck->countCards();
        emit sig_amountOfCardsChanged(ip, attendingPlayer->at(activePlayer)->playerDeck->countCards());

        //send signal that drawPile changed
        qDebug() << "[SM - DrawCS] emitting new amount for drawPile with " << drawPile->countCards();
        emit sig_drawPileAmountChanged(drawPile->countCards());

        //evaluate card
        if (drawnCard == AbstractCard::ExplosiveKitten){
            emit sig_playerPlayedCard(ip, blaVector);
            nextState = nextState_B;    //should be defuse state
        }
    }


    setStateToSwitchState = true;
}

void DrawCardState::stateCleanUp(){}

DrawCardState::~DrawCardState(){}


void DefuseCardState::gameLogic(){
    //    qDebug() << "[SM DefCS] processing logic inside " << QString::fromStdString(this->name);

    //set default nextState to lost state
    nextState = nextState_A;    //should be lost state
}

void DefuseCardState::stateCleanUp(){}

void DefuseCardState::slt_playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards){
    if (stateActive){
        //Card played received from network
        qDebug() << "[SM - DefCS] - played card received from " << ip;

        //check if ip is active player
        if (ip == attendingPlayer->at(activePlayer)->getIPAddress()){

            //check if card is "defuse card"
            if(cards.size() == 1 && cards.at(0) == AbstractCard::Defuse){
                //check ip from "defuser" and if he has that card on its stack
                //todo

                //add card into discardpile ? -> now played cards pile, later when round finished then to the discard
                //playedCards->addCard(AbstractCard::Defuse);   //old version
                playedCards->push_back(new PlayedCard(AbstractCard::Defuse, ip));

                //send nope to all clients
                qDebug() << "[SM - DefCS] send clients the played cards";
                emit sig_playerPlayedCard(ip, cards);

                //send the new card amount to the other players GUI
                qDebug() << "[SM - DefCS] emitting new amount for " << ip << " with " << attendingPlayer->at(activePlayer)->playerDeck->countCards();
                emit sig_amountOfCardsChanged(ip, attendingPlayer->at(activePlayer)->playerDeck->countCards());

                nextState = nextState_B;    //should be switch player state
            }
            else if (cards.size() == 0){ //player sent empty card which means no defuse wants to be played
                nextState = nextState_A;
            }

            setStateToSwitchState = true;   //abort countdown and switch state
        }
    }
}

DefuseCardState::~DefuseCardState() {}

void PlaceKittenState::gameLogic(){
    //    qDebug() << "[SM PKS] processing logic inside " << QString::fromStdString(this->name);

    //what happens if no card is sent ?
    //maybe just place kitten on random possition ;)

}

void PlaceKittenState::stateCleanUp(){}

void PlaceKittenState::slt_placeKittenReceived(QString ip, int position){
    if (stateActive){
        //Card played received from network
        qDebug() << "[SM PKS] - played card received from " << ip;


        //check if ip is active player
        if (ip == attendingPlayer->at(activePlayer)->getIPAddress()){

            //put it into the draw pile
            qDebug() << "[SM PKS] - add Kitten @ " << position;
            drawPile->addKittenAtPosition(AbstractCard::ExplosiveKitten, position);

            //remove it from the player deck
            attendingPlayer->at(activePlayer)->playerDeck->getSpecificCard(AbstractCard::ExplosiveKitten);

            //amount of cards in drawPile changed
            //send signal that drawPile changed
            qDebug() << "[SM PKS] emitting new amount for drawPile with " << drawPile->countCards();
            emit sig_drawPileAmountChanged(drawPile->countCards());

            //send the new card amount to the other players GUI
            qDebug() << "[SM PKS] emitting new amount for " << ip << " with " << attendingPlayer->at(activePlayer)->playerDeck->countCards();
            emit sig_amountOfCardsChanged(ip, attendingPlayer->at(activePlayer)->playerDeck->countCards());

            setStateToSwitchState = true;   //abort countdown and switch state
        }else{
            //wrong player ?
            qDebug() << "[SM PKS] received kitten from wrong ip, TODO SEND BACK";
        }
    }
}

PlaceKittenState::~PlaceKittenState() {}


void PlayerLostState::gameLogic(){
    qDebug() << "[SM PLS] processing logic inside " << this->name;
    //int activePlayer = BaseState::getActivePlayer();

    //set ative player in attending players to lost and refresh playerlist on client side
    attendingPlayer->at(activePlayer)->setPlayerLost();
    emit sig_playerLost(attendingPlayer->at(activePlayer)->getIPAddress());

    //check if there are more then 1 player left
    int playersLeft = 0;
    for(auto iter = attendingPlayer->begin(); iter != attendingPlayer->end(); ++iter){
        if(!((*iter)->getPlayerLost()))
            ++playersLeft;
    }

    if (playersLeft > 1)
        nextState = nextState_A;
    else
        nextState = nextState_B;

    setStateToSwitchState = true;   //... and switch state

}

void PlayerLostState::stateCleanUp(){}

PlayerLostState::~PlayerLostState(){}


void EndGameState::gameLogic(){
    qDebug() << "[SM EGS] processing logic inside " << this->name;

    QString playerList = "";

    for(auto iter = attendingPlayer->begin(); iter != attendingPlayer->end(); ++iter){
        playerList.append((*iter)->getPlayerName() + ",");
    }

    //remove last comma from string
    playerList.chop(1);


    qDebug() << "[SM EGS] playerlist for statistics is " << playerList;

    //game Ended, emit signals to announce winner and close game, cleanup etc
    for(auto iter = attendingPlayer->begin(); iter != attendingPlayer->end(); ++iter){
        if(!((*iter)->getPlayerLost())){
            qDebug() << "[SM EGS] Winner is: " << (*iter)->getIPAddress();
            emit sig_announceWinner((*iter)->getIPAddress());

            //send game statistics


            //best of game
            //calculate the best OF...

            QMap<QString, unsigned int> mostDefuse;
            mostDefuse.insert("---", 0);

            QMap<QString, unsigned int> mostNope;
            mostNope.insert("---", 0);

            QMap<QString, unsigned int> mostCards;
            mostCards.insert("---", 0);

            QMap<QString, unsigned int> mostSpecialMoves;
            mostSpecialMoves.insert("---", 0);



            //parse every players log and send it to them by the way

            QMap<Logging::logVals, unsigned int> tempPlayerReport;

            for (auto iter = attendingPlayer->begin(); iter != attendingPlayer->end(); ++iter){
                //for(int i = 0; i < attendingPlayer->size(); ++i){

                //1. send it to the players
                tempPlayerReport = (*iter)->getReport();
                emit sig_sendPlayerStatistics((*iter)->getIPAddress(),(*iter)->getReport(), playerList);

                //2 read the values and write them into the global statistics

                //check mostDefuses
                if (mostDefuse.begin().value() < tempPlayerReport.value(Logging::amountDefuse)){
                    mostDefuse.clear();
                    mostDefuse.insert((*iter)->getPlayerName(),tempPlayerReport.value(Logging::amountDefuse));
                    qDebug() << "added new amount defuse highscore to gamestats";
                }

                //check mostNopes
                if (mostNope.begin().value() < tempPlayerReport.value(Logging::amountNope)){
                    mostNope.clear();
                    mostNope.insert((*iter)->getPlayerName(),tempPlayerReport.value(Logging::amountNope));
                    qDebug() << "added new amount nope highscore to gamestats";
                }

                //check mostCards
                if (mostCards.begin().value() < tempPlayerReport.value(Logging::amountCards)){
                    mostCards.clear();
                    mostCards.insert((*iter)->getPlayerName(),tempPlayerReport.value(Logging::amountCards));
                    qDebug() << "added new amount most cards highscore to gamestats";
                }

                //check mostSpecials
                if (mostSpecialMoves.begin().value() < tempPlayerReport.value(Logging::amountSpecialMoves)){
                    mostSpecialMoves.clear();
                    mostSpecialMoves.insert((*iter)->getPlayerName(),tempPlayerReport.value(Logging::amountSpecialMoves));
                    qDebug() << "added new amount special moves highscore to gamestats";
                }
            }

            //create return map
            QMap<Logging::logVals,QMap<QString, unsigned int>> gameBestOfStatistics;
            gameBestOfStatistics.insert(Logging::amountDefuse,mostDefuse);
            gameBestOfStatistics.insert(Logging::amountNope, mostNope);
            gameBestOfStatistics.insert(Logging::amountCards, mostCards);
            gameBestOfStatistics.insert(Logging::amountSpecialMoves, mostSpecialMoves);

            //...and send it
            //    void sig_sendGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int>> gameBestOfStatistics);

            emit sig_sendGameStatistics(gameBestOfStatistics);
        }
    }

    emit sig_gameEnded();
}

void EndGameState::stateCleanUp(){}

EndGameState::~EndGameState(){}

//testing
void EndGameState::connectTestSignals()
{
    connect(this, SIGNAL(sig_sendGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int>>)), this, SLOT(slt_getGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int>>)));
}

void EndGameState::slt_getGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int> > gameBestOfStatistics)
{
    this->gameBestOfStatistics = gameBestOfStatistics;
}
//end testing

//StateMachine from here


StateMachine::~StateMachine(){}

void StateMachine::gameLogic(){}

void StateMachine::stateCleanUp(){}


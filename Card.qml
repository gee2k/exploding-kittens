import QtQuick 2.0
import CardTypes 1.0

Item {
    property alias mouseArea1 : mouseArea1
    property alias image : image
    property alias countText: countText
    property alias countRect: rectangle1
    property alias rotationAnim: rotation
    property alias drawAnim: drawAnim
    property alias playAnim: playAnim

    id: card
    width: 597
    height: 822

    RotationAnimation{
        id: rotation
        running: false
        target: card
        property: "rotation"
        to: 1440
        duration: 500
        direction: RotationAnimation.Clockwise
    }

    ParallelAnimation {
        id: drawAnim
        running: false
        onStopped: card.destroy()

        NumberAnimation {
            target: card
            property: "x"
            to: (parent.width * 0.25)
            duration: 1000
        }

        NumberAnimation {
            target: card
            property: "y"
            to: (parent.height - parent.height * 0.25)
            duration: 1000
        }
    }

    ParallelAnimation {
        id: playAnim
        running: false
        onStopped: card.destroy()

        NumberAnimation {
            target: card
            property: "x"
            to: 0
            duration: 500
        }

        NumberAnimation {
            target: card
            property: "y"
            to: 0
            duration: 500
        }
    }

    Image {
        id: image
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: "qrc:///defuseCard"

        Rectangle {
            id: rectangle1
            x: 0
            y: 0
            width: parent.width * 0.25
            height: parent.width * 0.25
            color: "#ffffff"
            radius: 14
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            border.width: 2
            visible: true

            Text {
                id: countText
                text: "0"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                font.pixelSize: 12
            }
        }
    }

    MouseArea {
        id: mouseArea1
        anchors.fill: parent
    }

    /*
        \qmlmethod changeBG(type)

        Changes the image of the card based on the parameter type
    */
    function changeBG(type) {
        var source = "qrc:///loader";

        switch(type) {
            case CardType.Defuse: source = "qrc:///defuseCard"; break;
            case CardType.Attack: source = "qrc:///attackCard"; break;
            case CardType.Favor: source = "qrc:///favorCard"; break;
            case CardType.ExplosiveKitten: source = "qrc:///ExplodingKittenCard"; break;
            case CardType.Nope: source = "qrc:///nopeCard"; break;
            case CardType.Skip: source = "qrc:///skipCard"; break;
            case CardType.Shuffle: source = "qrc:///shuffleCard"; break;
            case CardType.SeeTheFuture: source = "qrc:///seeTheFutureCard"; break;
            case CardType.MommaCat: source = "qrc:///mommaCatCard"; break;
            case CardType.ShyBladder: source = "qrc:///shyBladderCatCard"; break;
            case CardType.ZombieCat: source = "qrc:///zombieCatCard"; break;
            case CardType.BikiniCat: source = "qrc:///bikiniCatCard"; break;
            case CardType.CatsSchroedinger: source = "qrc:///catsSchroedingerCard"; break;
            default: break;
        }

        image.source = source;
    }

    /*
        \qmlmethod changeCount(i)

        This method is used to change the counttext of the card
    */
    function changeCount(i) {
        countText.text = i;
    }

    /*
        \qmlmethod addToCount(i)

        This method chages the shown count to i
    */
    function addToCount(i) {
        countText.text = countText.text + i;
    }

    /*
        \qmlmethod incrementCount()

        This method increments the count text by one
    */
    function incrementCount() {
        countText.text = +countText.text + 1;
    }

    /*
        \qmlmethod decrementCount()

        This method decrements the count text by one
    */
    function decrementCount() {
        if(countText.text != "0") {
            countText.text = countText.text - 1;
        }
    }

    /*
        \qmlmethod resetCount()

        This method resets the count text to 0
    */
    function resetCount() {
        countText.text = 0;
    }

    /*
        \qmlmethod countEmpty()

        This method returns true when the count text is 0
    */
    function countEmpty() {
        if(countText.text == "0") {
            return true;
        } else {
            return false;
        }
    }

    /*
        \qmlmethod hide()

        This method hides the card
    */
    function hide() {
        card.visible = false;
    }

    /*
        \qmlmethod show()

        This method shows the card
    */
    function show() {
        card.visible = true;
    }
}

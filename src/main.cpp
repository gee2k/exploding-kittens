#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QQmlContext>
#include "gameadapter.h"
#include "deckTest.h"
#include "emptytest.h"
#include "dbhelpertest.h"
#include "gamerulestest.h"
#include <QTest>
#include <QObject>


bool testing = false;

int main(int argc, char *argv[])
{

    //initialize rand once
//    srand(time(NULL));
        srand(static_cast<int>(time(NULL)));

    AbstractCard::declareQML();
    Audio::declareQML();

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    //expose Properties to QML
    gameAdapter myGameAdapter;
    engine.rootContext()->setContextProperty("gameAdapter", &myGameAdapter);

    //load qml
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    engine.rootContext()->setContextProperty("app_path", "file:///"+app.applicationDirPath()+"/");

    QObject *mainMenu = engine.rootObjects().first()->children().first();
    qDebug() << "mainMenu " << mainMenu;

    //connect signals/slots
    QObject::connect(&myGameAdapter, SIGNAL(sig_createGameForm()), mainMenu, SLOT(createGameForm()));
    QObject::connect(&myGameAdapter, SIGNAL(sig_newPlayerInLobby(QString,QString)), mainMenu, SIGNAL(newPlayerInLobby(QString, QString)));
    QObject::connect(&myGameAdapter, SIGNAL(sig_playerLeftLobby(QString,QString)), mainMenu, SIGNAL(playerLeftLobby(QString, QString)));
    QObject::connect(&myGameAdapter, SIGNAL(sig_errorMessage(QString)), mainMenu, SIGNAL(newErrorMessage(QString)));
    QObject::connect(&myGameAdapter, SIGNAL(sig_playedCards(QString,QVariant)), mainMenu, SIGNAL(newPlayedCards(QString, QVariant)));
    QObject::connect(&myGameAdapter, SIGNAL(sig_dbHelperInit()), mainMenu, SLOT(getActive()));
    QObject::connect(&myGameAdapter, SIGNAL(sig_localPlayersTurnBegun()), mainMenu, SLOT(cardsPlayable()));
    QObject::connect(&myGameAdapter, SIGNAL(sig_localPlayersTurnEnded()), mainMenu, SLOT(nonePlayable()));
    QObject::connect(&myGameAdapter, SIGNAL(sig_coplayersCardsOnHand(QString,QString)), mainMenu, SIGNAL(newPlayerCardCount(QString, QString)));
    QObject::connect(&myGameAdapter, SIGNAL(qmlPlayedCardChanged()), mainMenu, SLOT(changeLastPlayedCard()));
    QObject::connect(&myGameAdapter, SIGNAL(sig_seeTheFuture(QVariant)), mainMenu, SIGNAL(whatLiesBelow(QVariant)));
    QObject::connect(&myGameAdapter, SIGNAL(sig_explodingKittenDrawn()), mainMenu, SIGNAL(explodingKittenDrawn()));
    QObject::connect(&myGameAdapter, SIGNAL(sig_placeKitten()), mainMenu, SIGNAL(chooseKittenPos()));
    //QObject::connect(&myGameAdapter, SIGNAL(sig_drawPileShuffled()), mainMenu, SIGNAL(shuffle()));
    QObject::connect(&myGameAdapter, SIGNAL(sig_shuffle()), mainMenu, SIGNAL(shuffle()));
    QObject::connect(&myGameAdapter, SIGNAL(qmlDrawnCardChanged()), mainMenu, SIGNAL(draw()));
    QObject::connect(&myGameAdapter, SIGNAL(newActivePlayer(QString)), mainMenu, SIGNAL(newActivePlayer(QString)));
    QObject::connect(&myGameAdapter, SIGNAL(gameStatsReceived(QVariant)), mainMenu, SIGNAL(newGameStats(QVariant)));
    QObject::connect(&myGameAdapter, SIGNAL(sig_playerLost(QString)), mainMenu, SIGNAL(playerLost(QString)));
    QObject::connect(&myGameAdapter, SIGNAL(sig_progressChanged(double)), mainMenu, SIGNAL(progressChanged(double)));
    QObject::connect(&myGameAdapter, SIGNAL(sig_gameEnded()), mainMenu, SLOT(deleteGameForm()));
    emit myGameAdapter.sig_dbHelperInit();

    QUrl appPath(QString("%1").arg(app.applicationDirPath()));
    qDebug() << "AppPath is : " << appPath;

    if (testing){
        decktest testelicious;
        emptyTest emptytest;
        dbhelperTest dbtest;
        gameRulesTest gamerulestest;
        return (QTest::qExec(&emptytest) | QTest::qExec(&testelicious) | QTest::qExec(&dbtest)| QTest::qExec(&gamerulestest));
//        return (QTest::qExec(&gamerulestest));
    }
    else{
        return app.exec();
    }

}

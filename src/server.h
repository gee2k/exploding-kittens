#ifndef SERVER_H
#define SERVER_H

#include "QTcpServer"
#include "QTcpSocket"
#include "QDataStream"
#include "vector"
#include "QDebug"
#include "networkheaders.h"
#include "cards.h"
#include "player.h"
#include "stateMachine.h"
#include "logging.h"

/**
 * @brief MAXPLAYERCOUNT is the player limit
 */
static const unsigned int MAXPLAYERCOUNT = 5;

/**
 * @brief The Server class
 *
 * This class represents the server which listens for incomming connections,
 * stores a list of all players and sends and receives messages from and to clients
 * and also from and to the host gameplay.
 */
class Server : public QObject
{
    Q_OBJECT
public:

    /**
     * @brief Server is the constructor of this class
     * @param parent is the parent object of this object
     */
    explicit Server(QObject * parent = Q_NULLPTR);
    /**
     * @brief ~Server is the destructor of this class
     */
    ~Server();

    /**
     * @brief main method for sending messages
     *
     * Is used by every slot which transforms game data into a network message
     * which has to be sent to all clients
     * @param buffer the message to be sent
     */
    void sendNetworkSignal(QByteArray &buffer);

    /**
     * @brief method for sending messages to one specific player
     *
     * Is used by every slot which transforms game data into a network message
     * which has to be sent to one specific clients
     * @param buffer the message to be sent
     * @param ip is the ip address of the player who should get the message
     */
    void sendNetworkSignalTo(QString ip, QByteArray &buffer);
public slots:

    /**
     * @brief adds client sockets to the clients vector and connects signals
     */
    void acceptClient();

    /**
     * @brief main method for reading incomming messages
     *
     * Is used to read incomming network messages and emit signals
     * based on the content
     */
    void readNetworkSignal();

    /**
     * @brief sends the current game state to all players
     * @param state name of the game state
     */
    void stateChanged(BaseState::playStates state);   //connected to statemachine (every state)

    /**
     * @brief sends the player list to all clients
     * @param players list with all players
     */
    void sendPlayerList(std::vector<publicPlayer*>* players);   //connected from GA

    /**
     * @brief sends the player whos turn has started to everybody
     * @param ip the current players ip address
     */
    void sendActivePlayer(QString ip);  //conected from SM(SPS)

    /**
     * @brief sends a message to all client when the host starts the game
     */
    void sendGameIsStarting();  //connected from GA

    /**
     * @brief deletes disconnected player from the socket list
     *
     * Is triggered when a client socket sends a disconnected signal
     */
    void disconnectPlayer();

    /**
     * @brief sends the player who has to be kicked from the lobby
     * @param ip the players ip address who has to be kicked
     */
    void kickPlayer(QString ip);

    /**
     * @brief sends the winning player to everybody
     * @param ip ip address of the winner
     */
    void announceWinner(QString ip);    //connected to SM(EGS)

    /**
     * @brief sends a message everytime a player explodes
     * @param ip ip address of the looser
     */
    void announceLooser(QString ip);    //connected from SM(PLS)

    /**
     * @brief The Deck a Player gets at the beginning or The Card which is draw during draw state
     * @param ip the players ip address
     * @param cards the cards the player gets
     */
    void slt_sendCards(QString ip, std::vector<AbstractCard::CardTypes> cards); //connected from GP, SM(DCS)

    /**
     * @brief slt_sendCard is the easier form of the vector version of sending cards to player via ip
     * @param ip of the receiving player
     * @param cardType is the card which has to be sent
     */
    void slt_sendCard(QString ip, AbstractCard::CardTypes cardType);

    /**
     * @brief The Cards a Player played during playState to show in the gui (via game adapter)
     * @param ip the players ip address
     * @param cards the cards the player played
     */
    void slt_sendPlayedCards(QString ip, std::vector<AbstractCard::CardTypes> cards);   //connected from SM(PCS)

    /**
     * @brief the game has ended (e.g. when the winner is anounced)
     */
    void slt_sendGameEnded();   //connected from SM(EGS)

    /**
     * @brief slt_amountOfCardsChanged is called from the statemachine when the cards on a players hand change
     */
    void slt_amountOfCardsChanged(QString ip, uint amount); //connected from SM(all)

    /**
     * @brief slt_drawPileCountChanged is called when the amount of cards in draw pile changes
     * @param amount
     */
    void slt_drawPileCountChanged(uint amount);

    /**
     * @brief slt_sendSeeTheFutureCards sends the see the future cards to the player playing the card
     * @param ip of the player
     * @param cards containing the peek cards
     */
    void slt_sendSeeTheFutureCards(QString ip, std::vector<AbstractCard::CardTypes> cards); //connected from SM(ACS)

    /**
     * @brief slt_sendDeckShuffled is send when shuffle was palyed or the deck was adjustet due player leave
     */
    void slt_sendDeckShuffled();

    /**
     * @brief slt_clientDisconnected is emitted when a client shuts its connection
     */
    void slt_clientDisconnected();

    /**
     * @brief closeSocket casts sender of the signal to QTcpSocket and uses its close() method to close connection and delete it
     */
    void closeSocket();

    /**
     * @brief slt_sendPlayerStatistics sends the logged statistics from the game to the player via network
     * @param ip is the ip of the play who should get the statistics
     * @param playerStatistics are the statistics of the player stored in a map
     * @param playerList is a list of the players from the finished match
     */
    void slt_sendPlayerStatistics(QString ip, QMap<Logging::logVals, unsigned int> playerStatistics, QString playerList);


    /**
     * @brief slt_sendGameStatistics sends the bestOf from the game to every client as end Screen info
     * @param gameBestOfStatistics of the game
     */
    void slt_sendGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int> > gameBestOfStatistics);

    /**
     * @brief slt_sendProgress sends the remaining time the current state in percentage
     * @param percentage the remaining time in percentage from 0 to 1
     */
    void slt_sendProgress(double percentage);

    /**
     * @brief slt_removeCards the card a player loses in play to other players
     * @param ip the player losing the card
     * @param cards containing the card to remove
     */
    void slt_removeCards(QString ip, std::vector<AbstractCard::CardTypes> cards);

signals:

    /**
     * @brief emitted when a player joins
     * @param name the joining players nickname
     * @param ip the joining players ip address
     */
    void addPlayer(QString name, QString ip);   //connected to GA

    /**
     * @brief emitted when a CLIENT has played some cards
     * @param ip the players ip address
     * @param cards the cards the player has played
     */
    void clientPlayedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards); //connected to SM(PCS), SM(GNS), SM(DCS)

    /**
     * @brief The place in the drawpile where the kitten should be placed
     * @param ip the players ip address
     * @param position the position inside the drawpile
     */
    void sig_placeKitten(QString ip, int position); //connected to SM(PKS)

    /**
     * @brief sig_playerDisconnectedFromGame emitted when a player leaves the game (Lobby or ingame)
     * @param ip of the player leaving
     */
    void sig_playerDisconnectedFromGame(QString ip);
private:

    /**
     * @brief the server which accepts and maintains tcp connections
     */
    QTcpServer *server;

    /**
     * @brief the port for communication
     */
    quint16 port = 1337; //LEEEEEEET

    /**
     * @brief the list of connected client sockets
     */
    std::vector<QTcpSocket*> *clients;

    /**
     * @brief Deletes the client socket with matching ip from clients
     * @param ip the ip of the socket to delete
     */
    void deleteClientSocket(QString ip);

    /**
     * @brief inLobby variable to check if a game is running or the players are in the lobby
     */
    bool inLobby;
};

#endif // SERVER_H

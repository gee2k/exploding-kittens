import QtQuick 2.0
import CardTypes 1.0
import SoundTypes 1.0

Item {
    id: gameform
    anchors.fill: parent

    property alias defuseCard : defuseCard
    property alias attackCard : attackCard
    property alias favorCard : favorCard
    property alias nopeCard : nopeCard
    property alias skipCard : skipCard
    property alias shuffleCard : shuffleCard
    property alias seeTheFutureCard : seeTheFutureCard
    property alias catsSchroedingerCard : catsSchroedingerCard
    property alias mommaCatCard : mommaCatCard
    property alias shyBladderCatCard : shyBladderCatCard
    property alias zombieCatCard : zombieCatCard
    property alias bikiniCatCard : bikiniCatCard
    property alias drawpile: drawpile

    property var playerComponents: [];

    property var progressRedLimit: gameform.width * 0.3;

    property var isH: ;

    /*
        \qmlmethod updateProgressBar(percent)

        This method updates the width of the progress bar based on the parameter percent
    */
    function updateProgressBar(percent) {
        timeprogress.width = gameform.width * percent;
        if(timeprogress.width < progressRedLimit) {
            timeprogress.color = "#99d14141";
        } else {
            timeprogress.color = "#99bada55";
        }
    }

    /*
        \qmlmethod showDrawn()

        This method creates a new card item which slides in an animation the the player deck and plays a sound
    */
    function showDrawn() {
        var x = drawpile.x;
        var wid = drawpile.width;
        var hei = drawpile.height;
        var component = Qt.createComponent("qrc:///Card.qml");
        var cardComponent = component.createObject(gameform, {"countRect.visible": false, "x": x, "width": wid, "height": hei});

        if(cardComponent == null) {console.log("card not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
        cardComponent.changeBG(gameAdapter.qmlDrawnCard);
        cardComponent.drawAnim.running = true;
        gameAdapter.slt_playSound(Sound.Play3);
    }

    /*
        \qmlmethod animatePlayed(type)

        This method creates a new card item which slides to the CardsToBePlayed form in an animation
    */
    function animatePlayed(type) {
        var coords = type.mapToItem(gameform, 0, 0);
        var wid = type.width;
        var hei = type.height;
        var img = type.image.source;
        var component = Qt.createComponent("qrc:///Card.qml");
        var cardComponent = component.createObject(gameform, {"countRect.visible": false, "x": coords.x, "y": coords.y, "width": wid, "height": hei, "image.source": img});

        if(cardComponent == null) {console.log("card not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
        cardComponent.playAnim.running = true;
        //gameAdapter.slt_playSound(Sound.Play1);
    }

    /*
        \qmlmethod markLooser(ip)

        This method changes the text color of the player with the ip from parameter
    */
    function markLooser(ip) {
        if(playerComponents != null) {
            for(var i = 0; i < playerComponents.length; i++) {
                if(playerComponents[i].ip == ip) {
                    playerComponents[i].playername.color = "#d14141";
                    break;
                }
            }
        }
    }

    /*
        \qmlmethod updateActivePlayer(ip)

        This method changes the background color of the player item with the ip from parameter to show
        who's turn it is
    */
    function updateActivePlayer(ip) {
        if(playerComponents != null) {
            for(var i = 0; i < playerComponents.length; i++) {
                if(playerComponents[i].ip == ip) {
                    playerComponents[i].color = "#99bada55";
                } else {
                    playerComponents[i].color = "#99ffffff";
                }
            }
        }
    }

    /*
        \qmlmethod changeLastPlayedCard()

        This method updates the card image of the last played card item
    */
    function changeLastPlayedCard() {
        playedpile.changeBG(gameAdapter.qmlPlayedCard);
    }

    /*
        \qmlmethod changePlayerCardCount(ip, amount)

        This method updates the amount of cards in the player item with the ip from param
    */
    function changePlayerCardCount(ip, amount) {
        if(playerComponents != null) {
            for(var i = 0; i < playerComponents.length; i++) {
                if(playerComponents[i].ip == ip) {
                    playerComponents[i].setCardCount(amount);
                    break;
                }
            }
        }
    }

    /*
        \qmlmethod addPlayer(name, ip)

        This method creates a new player item and saves it in the playerComponents list
    */
    function addPlayer(name, ip) {
        var component = Qt.createComponent("qrc:///Player.qml");
        var player = component.createObject(column1, {"ip": ip, "playername.text": name});
        playerComponents.push(player);

        if(player == null) {console.log("Player not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
    }

    /*
        \qmlmethod deletePlayer(name, ip)

        This method deletes the playeritem with the same name and ip from parameters given
    */
    function deletePlayer(name, ip) {
        if(playerComponents != null) {
            for(var i = 0; i < playerComponents.length; i++) {
                if(playerComponents[i].ip == ip) {
                    console.log("GUI delete player index: " + i);
                    playerComponents[i].destroyThis();
                    delete playerComponents[i];
                    playerComponents.sort();
                    playerComponents.pop();
                    break;
                }
            }
        }
    }

    /*
        \qmlmethod hide()

        This method hides the game form(obsolete)
    */
    function hide() {
        gameform.visible = false;
    }

    /*
        \qmlmethod show()

        This method shows the game form(obsolete)
    */
    function show() {
        gameform.visible = true;
    }

    /*
        \qmlmethod destroyThis()

        This method destroys the game form and plays the right loop
    */
    function destroyThis() {
        gameform.destroy();
        gameAdapter.slt_playLoop(Sound.MenuLoop);
    }

    /*
        \qmlmethod onlyNope()

        This method greys out all cards except the nope card
    */
    function onlyNope() {
        inactiveCardsLayer.visible = true;
        inactiveCardsLayerNope.visible = false;
    }

    /*
        \qmlmethod allCards()

        This method makes all cards playable
    */
    function allCards() {
        inactiveCardsLayer.visible = false;
        inactiveCardsLayerNope.visible = false;
    }

    /*
        \qmlmethod noCards()

        This method greys out all cards
    */
    function noCards() {
        inactiveCardsLayerNope.visible = true;
        inactiveCardsLayer.visible = true;
    }

    /*
        \qmlmethod cardsToBePlayedHidden()

        This method returns true if the CardsToBePlayed form is hidden
    */
    function cardsToBePlayedHidden() {
        if(cardsToBePlayed1.visible == false) {
            return true;
        } else {
            return false;
        }
    }

    /*
        \qmlmethod selectCard(type, name)

        This method adds the selected card to buffer and shows it in CardsToBePlayed form
    */
    function selectCard(type, name) {
        console.log(name);
        if(cardsToBePlayedHidden && type.countText.text != 0) {
            animatePlayed(type);
            gameAdapter.slt_playSound(Sound.Play1);
            cardsToBePlayed1.show();
        }

        if(!type.countEmpty()) {
            if(name === "defuseCard") {
                cardsToBePlayed1.defuseCard.show();
                cardsToBePlayed1.defuseCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.Defuse);
            }
            if(name === "attackCard") {
                cardsToBePlayed1.attackCard.show();
                cardsToBePlayed1.attackCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.Attack);
            }
            if(name === "favorCard") {
                cardsToBePlayed1.favorCard.show();
                cardsToBePlayed1.favorCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.Favor);
            }
            if(name === "nopeCard") {
                cardsToBePlayed1.nopeCard.show();
                cardsToBePlayed1.nopeCard.incrementCount();
                console.log(CardType.Nope);
                gameAdapter.slt_addCardToBePlayed(CardType.Nope);
            }
            if(name === "skipCard") {
                cardsToBePlayed1.skipCard.show();
                cardsToBePlayed1.skipCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.Skip);
            }
            if(name === "shuffleCard") {
                cardsToBePlayed1.shuffleCard.show();
                cardsToBePlayed1.shuffleCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.Shuffle);
            }
            if(name === "seeTheFutureCard") {
                cardsToBePlayed1.seeTheFutureCard.show();
                cardsToBePlayed1.seeTheFutureCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.SeeTheFuture);
            }
            if(name === "catsSchroedingerCard") {
                cardsToBePlayed1.catsSchroedingerCard.show();
                cardsToBePlayed1.catsSchroedingerCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.CatsSchroedinger);
            }
            if(name === "mommaCatCard") {
                cardsToBePlayed1.mommaCatCard.show();
                cardsToBePlayed1.mommaCatCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.MommaCat);
            }
            if(name === "shyBladderCatCard") {
                cardsToBePlayed1.shyBladderCatCard.show();
                cardsToBePlayed1.shyBladderCatCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.ShyBladder);
            }
            if(name === "zombieCatCard") {
                cardsToBePlayed1.zombieCatCard.show();
                cardsToBePlayed1.zombieCatCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.ZombieCat);
            }
            if(name === "bikiniCatCard") {
                cardsToBePlayed1.bikiniCatCard.show();
                cardsToBePlayed1.bikiniCatCard.incrementCount();
                gameAdapter.slt_addCardToBePlayed(CardType.BikiniCat);
            }
            //type.decrementCount();
        }
    }

    Rectangle {
        id: pilesAndInfo
        color: "#99000000"
        width: parent.width
        height: parent.height / 2
        visible: true
        anchors.top: parent.top
        anchors.topMargin: 0

        Rectangle {
            id: decks
            width: parent.width *0.66
            height: parent.height
            color: "#00000000"

            Row {
                id: piles
                spacing: 20
                anchors.fill: parent

                Card {
                    id: drawpile
                    width: parent.width / 2
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    countText.text: gameAdapter.amountOfCardsInDrawPile
                    image.source: "qrc:///Deck"
                    mouseArea1.onClicked: {
                        gameAdapter.playCards();
                    }
                }

                Card {
                    id: playedpile
                    width: parent.width / 2
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    countRect.visible: false
                }
            }

            CardsToBePlayed {
                id: cardsToBePlayed1
                anchors.fill: parent
                visible: false

                function show() {
                    cardsToBePlayed1.visible = true;
                }

                function hide() {
                    cardsToBePlayed1.visible = false;
                }
            }
        }

        Rectangle {
            id: players
            width: parent.width *0.34
            height: parent.height
            color: "#00000000"
            anchors.right: parent.right
            anchors.rightMargin: 0

            Column {
                id: column1
                width: parent.width
                height: parent.height

                /*Player {
                    id: player1
                    width: parent.width
                    height: parent.height * 0.2
                }

                Player {
                    id: player2
                    width: parent.width
                    height: parent.height * 0.2
                }

                Player {
                    id: player3
                    width: parent.width
                    height: parent.height * 0.2
                }

                Player {
                    id: player4
                    width: parent.width
                    height: parent.height * 0.2
                }*/

                Rectangle {
                    id: quitSession
                    width: parent.width
                    height: parent.height * 0.166
                    color: mouseArea1.containsMouse ? "#99ffffff" : "#99d14141"
                    //border.width: 2
                    //border.color: "#c70b0b"

                    Text {
                        id: text1
                        color: "#ededed"
                        text: qsTr("Quit Session") + gameAdapter.translation
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.fill: parent
                        font.pixelSize: 22
                        font.bold: true
                    }

                    MouseArea {
                        id: mouseArea1
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked: {
                            gameAdapter.slt_playSound(Sound.Select3);
                            gameAdapter.clientLeaveGame();
                            destroyThis();
                            main_form.gameformComponent = null;
                            main_form.state = "";
                        }
                        onContainsMouseChanged: {
                            if(mouseArea1.containsMouse) gameAdapter.slt_playSound(Sound.Select2);
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: timeprogress
        color: "#99bada55"
        width: parent.width
        height: 20
        anchors.top: pilesAndInfo.bottom
        anchors.topMargin: -10
        anchors.left: parent.left
        anchors.leftMargin: 0
    }

    Rectangle {
        id: playerdeck
        color: "#99000000"
        width: parent.width
        height: parent.height / 2
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0

        Flickable {
            id: flickablePlayerCards
            width: parent.width
            height: parent.height
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.top: parent.top
            contentWidth: playerdeckCards.width + playerdeckCards0.width
            visible: true
            interactive: true
            contentHeight: parent.height
            boundsBehavior: Flickable.DragAndOvershootBounds
            flickableDirection: Flickable.HorizontalFlick

            Row {
                id: playerdeckCards0
                width: childrenRect.width
                height: parent.height
                spacing: 20
                anchors.left: parent.left
                anchors.leftMargin: 0

                Card {
                    id: nopeCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///nopeCard"
                    mouseArea1.onClicked: selectCard(this, "nopeCard")
                    countText.text: gameAdapter.qmlAmountNopeCards
                    //Component.onCompleted: this.changeCount(3)
                }
            }

            Rectangle {
                id: inactiveCardsLayerNope
                width: playerdeckCards0.width
                height: playerdeckCards0.height
                color: "#99000000"
                visible: true
                anchors.left: playerdeckCards0.left
                anchors.leftMargin: 0

                MouseArea {
                    id: inactiveCardsLayerNopeMouseArea
                    anchors.fill: parent
                }
            }

            Row {
                id: playerdeckCards
                width: childrenRect.width
                height: parent.height
                spacing: 20
                anchors.left: playerdeckCards0.right
                anchors.leftMargin: 0

                Card {
                    id: defuseCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    mouseArea1.onClicked: selectCard(this, "defuseCard")
                    countText.text: gameAdapter.qmlAmountDefuseCards
                    //Component.onCompleted: this.changeCount(3)
                    //mouseArea1.onClicked: gameAdapter.sendDemoCard()
                    //image.source: "qrc:///defuseCard"
                }

                Card {
                    id: attackCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///attackCard"
                    mouseArea1.onClicked: selectCard(this, "attackCard")
                    countText.text: gameAdapter.qmlAmountAttackCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: favorCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///favorCard"
                    mouseArea1.onClicked: selectCard(this, "favorCard")
                    countText.text: gameAdapter.qmlAmountFavorCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: skipCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///skipCard"
                    mouseArea1.onClicked: selectCard(this, "skipCard")
                    countText.text: gameAdapter.qmlAmountSkipCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: shuffleCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///shuffleCard"
                    mouseArea1.onClicked: selectCard(this, "shuffleCard")
                    countText.text: gameAdapter.qmlAmountShuffleCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: seeTheFutureCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///seeTheFutureCard"
                    mouseArea1.onClicked: selectCard(this, "seeTheFutureCard")
                    countText.text: gameAdapter.qmlAmountSeeTheFutureCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: catsSchroedingerCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///catsSchroedingerCard"
                    mouseArea1.onClicked: selectCard(this, "catsSchroedingerCard")
                    countText.text: gameAdapter.qmlAmountCatsSchroedingerCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: mommaCatCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///mommaCatCard"
                    mouseArea1.onClicked: selectCard(this, "mommaCatCard")
                    countText.text: gameAdapter.qmlAmountMommaCatCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: shyBladderCatCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///shyBladderCatCard"
                    mouseArea1.onClicked: selectCard(this, "shyBladderCatCard")
                    countText.text: gameAdapter.qmlAmountShyBladderCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: zombieCatCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///zombieCatCard"
                    mouseArea1.onClicked: selectCard(this, "zombieCatCard")
                    countText.text: gameAdapter.qmlAmountZombieCatCards
                    //Component.onCompleted: this.changeCount(3)
                }

                Card {
                    id: bikiniCatCard
                    width: 180
                    height: parent.height * 0.8
                    anchors.verticalCenter: parent.verticalCenter
                    image.source: "qrc:///bikiniCatCard"
                    mouseArea1.onClicked: selectCard(this, "bikiniCatCard")
                    countText.text: gameAdapter.qmlAmountBikiniCatCards
                    //Component.onCompleted: this.changeCount(3)
                }
            }

            Rectangle {
                id: inactiveCardsLayer
                width: playerdeckCards.width
                height: playerdeckCards.height
                color: "#99000000"
                anchors.left: playerdeckCards0.right
                anchors.leftMargin: 0
                visible: true

                MouseArea {
                    id: inactiveCardsLayerMouseArea
                    anchors.fill: parent
                }
            }
        }
    }

}

#ifndef LOGGING_H
#define LOGGING_H

#include "cards.h"
#include "vector"


/**
 * @brief The Logging class is set to hold the information which cards were played by the player for later statistics and XP calculation
 */
class Logging
{
public:

    /**
     * @brief The logVals enum sets the dataypes of the different logs
     */
    enum logVals{
        amountDefuse,
        amountNope,
        amountCards,
        amountSpecialMoves
    };

    //Q_ENUM(logVals)

private:

    /**
     * @brief log is the logging storage
     */
    QMap<Logging::logVals, unsigned int> log;

    /**
     * @brief firstNope is to determin if the player played an additional nope wich gives more xp in later examination
     */
    bool firstNope = true;

public:

    /**
     * @brief Logging constructor
     */
    Logging();

    /**
     * @brief counting class for logging of playedCards
     * @param card which has been played
     */
    void playedCard(AbstractCard::CardTypes card);

    /**
     * @brief addSpecialMove is incremented when a special play occures by a player
     */
    void addSpecialMove();

    /**
     * @brief finishedNopingRound resets the firstNope value so that consecutive played nopes can again be counted
     */
    void finishedNopingRound();

    /**
     * @brief getReport returns the final logfile of the player played cards
     * @return QMap<QString, unsigned int> where QString is the variable name and int the Value
     */
    QMap<Logging::logVals, unsigned int> getReport() const;
};

Q_DECLARE_METATYPE(Logging)

#endif // LOGGING_H

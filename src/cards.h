#ifndef CARDDECK_H
#define CARDDECK_H
#include <QObject>
#include <QMap>
#include <QtQml>

/**
 * @brief The AbstractCard class contains the info about the different cards of the game
 */
class AbstractCard : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief The CardTypes enum holds the different card types available in the game
     */
    enum CardTypes{
        Attack,             //4  0
        Defuse,             //6  1
        ExplosiveKitten,    //4  2
        Favor,              //4  3
        Nope,               //5  4
        SeeTheFuture,       //5  5
        Shuffle,            //4  6
        Skip,               //4  7
        BikiniCat,          //4  8
        CatsSchroedinger,   //4  9
        MommaCat,           //4  10
        ShyBladder,         //4  11
        ZombieCat,          //4  12
        ERRORCARD
    };

    Q_ENUM(CardTypes)

    /**
     * @brief declareQML
     */
    static void declareQML() {
        qmlRegisterType<AbstractCard>("CardTypes", 1, 0, "CardType");
    }

    /**
     * @brief AbstractCard constructor
     */
    AbstractCard();

    /**
     * @brief AbstractCard constructor
     * @param cardType of this card created
     */
    AbstractCard(AbstractCard::CardTypes cardType);

    /**
     * @brief getCardType getter for cardType
     * @return cardType
     */
    virtual AbstractCard::CardTypes getCardType() const;

    /**
     * @brief setCardType setter for cardType
     * @param value to set cardType tp
     */
    virtual void setCardType(const AbstractCard::CardTypes &value);

protected:
    /**
     * @brief m_cardType the type the card has
     */
    AbstractCard::CardTypes m_cardType;
};



/**
 * @brief The playedCard class contains the information about cards that are beeing played with their owner that in the event of a rollback the cards can easily be assigned
 */
class PlayedCard : public AbstractCard
{

public:

    /**
     * @brief playedCard constructor
     * @param cardType is the type of playCard
     * @param ownerIP ips of the owner
     */
    PlayedCard(AbstractCard::CardTypes cardType, QString ownerIP);

    /**
     * @brief getOwnerIP returns the card owner
     * @return QString of the playerIP
     */
    QString getOwnerIP() const;

    /**
     * @brief setOwnerIP sets the cardOwner of the Card
     * @param value QString as owner ip
     */
    void setOwnerIP(const QString &value);

private:

    /**
     * @brief ownerIP, yeah right that
     */
    QString ownerIP;
};

//Q_DECLARE_METATYPE(AbstractCard)
#endif // CARDDECK_H

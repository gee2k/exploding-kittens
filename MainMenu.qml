import QtQuick 2.4
import QtQuick.Window 2.2
import CardTypes 1.0
import SoundTypes 1.0

Item {

    anchors.fill: parent
    property alias menu_wrapper: menu_wrapper
    property alias main_form: main_form
    id: main_form
    property var gameformComponent;
    property var explodingKittenForm;

    signal newPlayerInLobby(string name, string ip);
    signal playerLeftLobby(string name, string ip);
    signal newErrorMessage(string message);
    signal newPlayedCards(string name, var cards);
    signal newPlayerCardCount(string ip, string amount);
    signal whatLiesBelow(var cards);
    signal explodingKittenDrawn();
    signal chooseKittenPos();
    signal shuffle();
    signal draw();
    signal newActivePlayer(string ip);
    signal newGameStats(var gameStats);
    signal playerLost(string ip);
    signal progressChanged(double percent);

    Connections {
        onNewPlayerInLobby: addPlayerToLobby(name, ip)
        onPlayerLeftLobby: deletePlayerFromLobby(name, ip)
        onNewErrorMessage: createErrorMessage(message)
        onNewPlayedCards: showPlayedCards(name, cards)
        onNewPlayerCardCount: changePlayerCardCount(ip, amount)
        onWhatLiesBelow: seeTheFuture(cards)
        onExplodingKittenDrawn: showExplodingKittenDrawn()
        onChooseKittenPos: showChooseKittenPos()
        onShuffle: showShuffle()
        onDraw: showDrawn();
        onNewActivePlayer: updateActivePlayer(ip)
        onPlayerLost: markLooser(ip)
        onNewGameStats: showGameStats(gameStats)
        onProgressChanged: updateProgressBar(percent)
    }

    Component.onCompleted: {
        loadSettings();
        intro.visible = true;
    }

    /*
        \qmlmethod updateProgressBar(percent)

        This method updates the progress bar in gameform
    */
    function updateProgressBar(percent) {
        if(gameformComponent != null) {
            gameformComponent.updateProgressBar(percent);
        }
    }

    /*
        \qmlmethod showGameStats(gameStats)

        This method shows the end game statistics received from the server by the and of a match
    */
    function showGameStats(gameStats) {
        var component = Qt.createComponent("qrc:///EndGameStats.qml");
        var statsComponent = component.createObject(main_form, {});

        if(statsComponent == null) {console.log("stats form not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
        statsComponent.loadStats(gameStats);
    }

    /*
        \qmlmethod loadSettingsName(name)

        This method updates the playername in the options menu
    */
    function loadSettingsName(name) {
        optionsMenuForm1.updateName(name);
    }

    /*
        \qmlmethod loadSettings()

        This method loads the settings in the options menu and sets windows mode
    */
    function loadSettings() {
        var settings = gameAdapter.getSettings();
        if(settings[settings.length - 2] == 1) {
            root.visibility = Window.FullScreen;
        }
        optionsMenuForm1.loadSettings(settings);
    }

    /*
        \qmlmethod markLooser(ip)

        This method marks a player who has lost according to te ip from parameter
    */
    function markLooser(ip) {
        if(gameformComponent != null) {
            gameformComponent.markLooser(ip);
        }
        deleteExplodingKittenForm();
    }

    /*
        \qmlmethod updateActivePlayer(ip)

        This method updates the currently active player in game form according to the ip
    */
    function updateActivePlayer(ip) {
        if(gameformComponent != null) {
            gameformComponent.updateActivePlayer(ip);
        }
    }

    /*
        \qmlmethod showChooseKittenPos()

        This method creates a form to choose a position where the kitten card should be placed
    */
    function showChooseKittenPos() {
        deleteExplodingKittenForm();
        var component = Qt.createComponent("qrc:///ChooseKittenPosition.qml");
        var explodingComponent = component.createObject(main_form, {});

        if(explodingComponent == null) {console.log("expk pos not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
        gameAdapter.slt_playLoop(Sound.DefuseLoop);
    }

    /*
        \qmlmethod showDrawn()

        This method shows the card which was drawn by the player
    */
    function showDrawn() {
        if(gameformComponent != null) {
            gameformComponent.showDrawn();
        }
    }

    /*
        \qmlmethod showShuffle()

        This method plays the shuffle animation
    */
    function showShuffle() {
        var component = Qt.createComponent("qrc:///Shuffle.qml");
        var shuffleComponent = component.createObject(main_form, {});

        if(shuffleComponent == null) {console.log("shuffle not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
    }

    /*
        \qmlmethod showExplodingKittenDrawn()

        This method creates the exploding kitten form to show that this card was drawn and
        give ability to play a defuse card
    */
    function showExplodingKittenDrawn() {
        var component = Qt.createComponent("qrc:///ExplodingKitten.qml");
        explodingKittenForm = component.createObject(main_form, {});

        if(explodingKittenForm == null) {console.log("expk not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
        gameAdapter.slt_playLoop(Sound.KittenDrawLoop);
    }

    /*
        \qmlmethod deleteExplodingKittenForm()

        This method deletes the exploding kitten form ones the player lost the game
    */
    function deleteExplodingKittenForm() {
        if(explodingKittenForm != null) {
            explodingKittenForm.destroy();
            explodingKittenForm = null;
        }
    }

    /*
        \qmlmethod changeLastPlayedCard()

        This method updates the last played card display in gameform
    */
    function changeLastPlayedCard() {
        if(gameformComponent != null) {
            gameformComponent.changeLastPlayedCard();
        }
    }

    /*
        \qmlmethod changePlayerCardCount(ip, amount)

        This method updates the card amount of the player with ip from parameter in the game form
    */
    function changePlayerCardCount(ip, amount) {
        if(gameformComponent != null) {
            gameformComponent.changePlayerCardCount(ip, amount);
        }
    }

    /*
        \qmlmethod nonePlayable()

        This method greys out all cards
    */
    function nonePlayable() {
        if(gameAdapter.qmlMatchRunning) {
            gameformComponent.noCards();
        }
    }

    /*
        \qmlmethod cardsPlayable()

        This method makes all cards or only nope card playable
    */
    function cardsPlayable() {
        if(gameAdapter.qmlMatchRunning) {
            if(gameAdapter.qmlNoping) {
                gameformComponent.onlyNope();
            } else {
                gameformComponent.allCards();
                gameAdapter.slt_playSound(Sound.AnnouncerYourTurn);
            }
        }
    }

    /*
        \qmlmethod getActive()

        This method gets the currently active profile to show statistics in statistics menu and
        creates a CreateProfile form if there is no profile
    */
    function getActive() {
        console.log("getActive: " + gameAdapter.getActiveProfile());
        if(gameAdapter.getActiveProfile() == "") {
            console.log("no profile detected");
            //gameAdapter.createLocalProfile("Ronny");
            var component = Qt.createComponent("qrc:///CreateProfile.qml");
            var profileComponent = component.createObject(main_form, {});

            if(profileComponent == null) {console.log("Message not created")}
            if( component.status == Component.Error )
                    console.debug("Error:"+ component.errorString() );
            profileComponent.z = 10;
        } else {
            console.log(gameAdapter.getActiveProfileStats());
            updateStatsTab();
        }
    }

    /*
        \qmlmethod updateStatsTab()

        This method updates the information in the statistics menu
    */
    function updateStatsTab(){
        var stats = gameAdapter.getActiveProfileStats();
        var until = qsTr("until") + gameAdapter.translation;
        var exp_until = stats[7] + " / " + stats[10] + " XP " + until + " level " + (stats[9] + 1);
        statisticsMenuForm1.stats_from.text = stats[0];
        statisticsMenuForm1.exp_until_next.text = exp_until;
        statisticsMenuForm1.level.text = qsTr("level ") + gameAdapter.translation + stats[9];
        statisticsMenuForm1.cards_played.text = stats[1];
        statisticsMenuForm1.wins.text = stats[2];
        statisticsMenuForm1.losses.text = stats[3];
        statisticsMenuForm1.defused.text = stats[4];
        statisticsMenuForm1.nopes.text = stats[5];
        statisticsMenuForm1.special.text = stats[6];
        statisticsMenuForm1.lfoes_content.text = stats[8];
        loadSettingsName(stats[0]);
    }

    /*
        \qmlmethod addPlayerToLobby(name, ip)

        This method adds a lobby player item to join or host lobby with name and ip from parameters
    */
    function addPlayerToLobby(name, ip) {

        console.log("addPlayerToLobby called! " + "isHost: " + gameAdapter.qmlIsHost);
        if(gameAdapter.qmlIsHost) {
            hostGameForm1.addHostLobbyPlayer(name, ip)
        } else {
            joinGameForm1.addJoinLobbyPlayer(name, ip);
        }
    }

    /*
        \qmlmethod deletePlayerFromLobby(name, ip)

        This method deletes the player from host or join lobby or gameform with name and ip from parameters
    */
    function deletePlayerFromLobby(name, ip) {
        if(gameAdapter.qmlMatchRunning) {
            gameformComponent.deletePlayer(name, ip);
        }
        if(gameAdapter.qmlIsHost) {
            hostGameForm1.deleteHostLobbyPlayer(ip);
        } else {
            joinGameForm1.deleteJoinLobbyPlayer(ip);
        }
    }

    /*
        \qmlmethod createErrorMessage(message)

        This method shows the error message which was received
    */
    function createErrorMessage(message) {
        console.log("createErrorMessage called #####################");
        var component = Qt.createComponent("qrc:///ErrorMessage.qml");
        var messageComponent = component.createObject(main_form, {"msg.text": message});

        if(messageComponent == null) {console.log("Message not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
    }

    /*
        \qmlmethod showPlayedCards(name, cards)

        This method show the cards which were played by a player
    */
    function showPlayedCards(name, cards) {
        console.log("showPlayedCards received!: name: " + name);
        var message = name + qsTr(" played:") + gameAdapter.translation;

        var component = Qt.createComponent("qrc:///PlayerPlayedCards.qml");
        var playedComponent = component.createObject(main_form, {"msg.text": message});

        if(playedComponent == null) {console.log("Form not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );

        for(var i = 0; i < cards.length; i++) {
            playedComponent.createCard(cards[i]);
            console.log(cards[i]);
        }

    }

    /*
        \qmlmethod seeTheFuture(cards)

        This method shows the cards received by playing a see the future card
    */
    function seeTheFuture(cards) {
        console.log("seeTheFuture received!");
        var message = qsTr("from top to bottom") + gameAdapter.translation;

        var component = Qt.createComponent("qrc:///PlayerPlayedCards.qml");
        var playedComponent = component.createObject(main_form, {"msg.text": message});

        if(playedComponent == null) {console.log("Form not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );

        for(var i = 0; i < cards.length; i++) {
            playedComponent.createCard(cards[i]);
            console.log(cards[i]);
        }

    }

    /*
        \qmlmethod createGameForm()

        This method creates a game form on match beginning
    */
    function createGameForm() {
        var component = Qt.createComponent("qrc:///GameForm.qml");
        gameformComponent = component.createObject(main_form, {/*"from": from*/});

        if(gameformComponent == null) {console.log("Gameform not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );

        if(gameAdapter.qmlIsHost) {
            for(var i = 0; i < hostGameForm1.players.length; i++) {
                if(hostGameForm1.players[i] != undefined) {
                    console.log(hostGameForm1.players[i].playername);
                    gameformComponent.addPlayer(hostGameForm1.players[i].playername.text, hostGameForm1.players[i].ip);
                }
            }
        } else {
            for(var i = 0; i < joinGameForm1.players.length; i++) {
                if(joinGameForm1.players[i] != undefined) {
                    console.log(joinGameForm1.players[i].playername);
                    gameformComponent.addPlayer(joinGameForm1.players[i].playername.text, joinGameForm1.players[i].ip);
                }
            }
        }
        gameformComponent.isH = gameAdapter.qmlIsHost;
        main_form.state = "bgOnly";
        gameAdapter.slt_playLoop(Sound.InGameLoop);
    }

    /*
        \qmlmethod deleteGameForm()

        This method deletes the gameform when a match has ended
    */
    function deleteGameForm() {
        if(gameformComponent != null) {
            gameformComponent.destroyThis();
            gameformComponent = null;
        }
        if(gameAdapter.qmlIsHost) {
            main_form.state = "Host Game";
        } else {
            main_form.state = "Join Game";
        }
        deleteExplodingKittenForm();
    }

    AnimatedImage {
        id: background_layer
        smooth: false
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        z: 0
        source: "qrc:///menuBG"
        Image {
            id: foreground_bg
            visible: false
            smooth: false
            fillMode: Image.PreserveAspectCrop
            anchors.fill: parent
            source: "qrc:///gameForeground"
        }
    }


    ParticleSystem {
        id: particle_layer
        smooth: true
        visible: true
        anchors.fill: parent
    }

    Image {
        id: gametitle
        visible: true
        smooth: false
        fillMode: Image.PreserveAspectCrop
        anchors.fill: parent
        source: "qrc:///gameTitle"
    }

    Rectangle {
        id: menu_wrapper
        height: parent.height * 0.1
        color: "#99ffffff"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        opacity: 1
        transformOrigin: Item.Center

        Row {
            id: row
            width: childrenRect.width
            height: childrenRect.height
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter//113
            spacing: parent.width * 0.016

            Rectangle {
                id: button0
                width: menu_wrapper.width * 0.18
                height: menu_wrapper.height * 0.8
                color: mouseArea.containsMouse ? "#99bada55" : "#99000000"

                Text {
                    id: text
                    color: "#ededed"
                    text: qsTr("Join Game") + gameAdapter.translation
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 22
                    font.bold: true
                }

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: main_form.state = "Join Game"
                    onContainsMouseChanged: {
                        if(mouseArea.containsMouse) gameAdapter.slt_playSound(Sound.Select2);
                    }
                }
            }

            Rectangle {
                id: button1
                width: menu_wrapper.width * 0.18
                height: menu_wrapper.height * 0.8
                color: mouseArea1.containsMouse ? "#99bada55" : "#99000000"
                Text {
                    id: text1
                    color: "#ededed"
                    text: qsTr("Host Game") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 22
                    font.bold: true
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                }

                MouseArea {
                    id: mouseArea1
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: main_form.state = "Host Game"
                    onContainsMouseChanged: {
                        if(mouseArea1.containsMouse) gameAdapter.slt_playSound(Sound.Select2);
                    }
                }
            }

            Rectangle {
                id: button2
                width: menu_wrapper.width * 0.18
                height: menu_wrapper.height * 0.8
                color: mouseArea2.containsMouse ? "#99bada55" : "#99000000"
                Text {
                    id: text2
                    color: "#ededed"
                    text: qsTr("Options") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 22
                    font.bold: true
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                }

                MouseArea {
                    id: mouseArea2
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: main_form.state = "Options"
                    onContainsMouseChanged: {
                        if(mouseArea2.containsMouse) gameAdapter.slt_playSound(Sound.Select2);
                    }
                }
            }

            Rectangle {
                id: button3
                width: menu_wrapper.width * 0.18
                height: menu_wrapper.height * 0.8
                color: mouseArea3.containsMouse ? "#99bada55" : "#99000000"
                Text {
                    id: text3
                    color: "#ededed"
                    text: qsTr("Statistics") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 22
                    font.bold: true
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                }

                MouseArea {
                    id: mouseArea3
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: main_form.state = "Statistics"
                    onContainsMouseChanged: {
                        if(mouseArea3.containsMouse) gameAdapter.slt_playSound(Sound.Select2);
                    }
                }
            }

            Rectangle {
                id: button4
                width: menu_wrapper.width * 0.18
                height: menu_wrapper.height * 0.8
                color: mouseArea4.containsMouse ? "#99d14141" : "#99000000"
                Text {
                    id: text4
                    color: "#ededed"
                    text: qsTr("Exit") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 22
                    font.bold: true
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                }

                MouseArea {
                    id: mouseArea4
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: Qt.quit()
                    onContainsMouseChanged: {
                        if(mouseArea4.containsMouse) gameAdapter.slt_playSound(Sound.Select3);
                    }
                }
            }
        }


    }

    Rectangle {
        id: side_menu
        color: "#99ffffff"
        anchors.left: menu_wrapper.horizontalCenter
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 40
        anchors.right: parent.right
        anchors.rightMargin: 40
        anchors.bottom: menu_wrapper.top
        anchors.bottomMargin: 40
        visible: false
        border.color: "#00000000"
        transformOrigin: Item.BottomRight
        opacity: 1

        JoinGameForm {
            id: joinGameForm1
            //objectName: joinGameForm1
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            visible: false
        }

        HostGameForm {
            id: hostGameForm1
            //objectName: hostGameForm1
            width: parent.width
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            visible: false
        }

        OptionsMenuForm {
            id: optionsMenuForm1
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            visible: false
        }

        StatisticsMenuForm {
            id: statisticsMenuForm1
            visible: false
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.top: parent.top
        }
    }

    AnimatedImage {
        id: intro
        z: 11
        paused: true
        smooth: false
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: "qrc:///IntroMovie"
        visible: false
        onPlayingChanged: {
            intro.destroy();
            gameAdapter.slt_stopSound();
            gameAdapter.slt_playLoop(Sound.MenuLoop);
        }
        onVisibleChanged: {
            intro.paused = false;
            gameAdapter.slt_playSound(Sound.Intro);
        }
        MouseArea {
            id: introSkip
            anchors.fill: parent
            hoverEnabled: true
            onClicked: intro.playing = false
        }
    }

    GameForm {
        id: gameForm1
        width: parent.width
        height: parent.height
        visible: false
    }

    states: [
        State {
            name: "Join Game"

            PropertyChanges {
                target: side_menu
                visible: true
            }

            PropertyChanges {
                target: joinGameForm1
                visible: true
            }

            PropertyChanges {
                target: gametitle
                visible: false
            }
        },
        State {
            name: "Host Game"

            PropertyChanges {
                target: side_menu
                visible: true
            }

            PropertyChanges {
                target: hostGameForm1
                visible: true
            }

            PropertyChanges {
                target: gametitle
                visible: false
            }
        },
        State {
            name: "Options"

            PropertyChanges {
                target: side_menu
                visible: true
            }

            PropertyChanges {
                target: optionsMenuForm1
                visible: true
            }

            PropertyChanges {
                target: gametitle
                visible: false
            }
        },
        State {
            name: "Statistics"

            PropertyChanges {
                target: statisticsMenuForm1
                visible: true
            }

            PropertyChanges {
                target: side_menu
                visible: true
            }

            PropertyChanges {
                target: gametitle
                visible: false
            }
        },
        State {
            name: "pressStartState"

            PropertyChanges {
                target: image1
                x: 270
                y: 130
                opacity: 1
            }
        },
        State {
            name: "showField"

            PropertyChanges {
                target: gameForm1
                visible: true
            }
        },
        State {
            name: "bgOnly"

            PropertyChanges {
                target: menu_wrapper
                visible: false
            }

            PropertyChanges {
                target: gametitle
                visible: false
            }
        }
    ]

}

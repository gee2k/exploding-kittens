#include "cards.h"

AbstractCard::AbstractCard()
{

}

AbstractCard::AbstractCard(CardTypes cardType):m_cardType(cardType){}


AbstractCard::CardTypes AbstractCard::getCardType() const
{
//    qDebug() << "[AbstractCard] return " << m_cardType;
    return this->m_cardType;
}

void AbstractCard::setCardType(const CardTypes &value)
{
    this->m_cardType = value;
}

//PlayedCard::PlayedCard(AbstractCard::CardTypes cardType, QString ownerIP):m_cardType(cardType), ownerIP(ownerIP){
//    qDebug() << "[PLAYCARD] created with: " << cardType << " owned by: " << ownerIP;
//}

PlayedCard::PlayedCard(AbstractCard::CardTypes cardType, QString ownerIP){
    this->m_cardType = cardType;
    this->ownerIP = ownerIP;
//    qDebug() << "[PLAYCARD] created with: " << cardType << " owned by: " << ownerIP;
}


QString PlayedCard::getOwnerIP() const
{
    return ownerIP;
}

void PlayedCard::setOwnerIP(const QString &value)
{
    ownerIP = value;
}

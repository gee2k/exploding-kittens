#include "gameplay.h"

//Q_DECLARE_SMART_POINTER_METATYPE(std::shared_ptr)


gamePlay::gamePlay(std::vector<privatePlayer*>* playerlist):attendingPlayer(playerlist){
    amountPlaying = playerlist->size();
}




void hostGameplay::start(){

    //create the statemachine
    stateMachine = new StateMachine();

    //connect signals for card changes

    bool cchng1 = connect(stateMachine, SIGNAL(sig_amountOfCardsChanged(QString,uint)), server, SLOT(slt_amountOfCardsChanged(QString,uint)));    //checked
    qDebug() << "[GP] cchng1 ? " << cchng1;

    bool cchng2 = connect(stateMachine, SIGNAL(sig_drawPileAmountChanged(uint)), server, SLOT(slt_drawPileCountChanged(uint)));    //checked
    qDebug() << "[GP] cchng2 ? " << cchng2;


    //first create the buffer
    cardBuffer = new std::vector<AbstractCard::CardTypes>;



    //create decks here
    m_drawPile = new DrawDeck(amountPlaying);

    //debug
//    m_drawPile = new DrawDeck();

//    for (int i = 0; i < amountPlaying; ++i)
//        m_drawPile->addCard(AbstractCard::ShyBladder);

    qDebug() << "[GP] Draw Pile generated";
    //connect send cards signal to network
    connect(this, SIGNAL(deckCreatedSendToPlayer(QString,std::vector<AbstractCard::CardTypes>)), server, SLOT(slt_sendCards(QString,std::vector<AbstractCard::CardTypes>)));    //checked

    //create amountPlaying decks for the players
    for (int i = 0; i < amountPlaying; ++i) {

        //create and assign a Deck to every Player
        attendingPlayer->at(i)->assignDeck(new PlayerDeck(m_drawPile));

        //put it into the buffer
        attendingPlayer->at(i)->playerDeck->readDeckToSendToNetwork(cardBuffer);

        //send it to the Player
        sendBufferToNetwork(attendingPlayer->at(i)->getIPAddress());

        //inform others of players card amount
        //send the new card amount to the other players GUI
        qDebug() << "[GP] emitting new amount for " << attendingPlayer->at(i)->getIPAddress() << " with " << attendingPlayer->at(i)->playerDeck->countCards();
        emit stateMachine->sig_amountOfCardsChanged(attendingPlayer->at(i)->getIPAddress(), attendingPlayer->at(i)->playerDeck->countCards());


        //clear buffer
        cardBuffer->clear();
    }
//    m_drawPile->addCard(AbstractCard::ExplosiveKitten);
    qDebug() << "[GP] Player Decks created";

    //finalize DrawPile
    m_drawPile->finalizeDeck();
    qDebug() << "[GP] drawpile finalized, has remaining cards: " << m_drawPile->countCards();
    emit stateMachine->sig_drawPileAmountChanged(m_drawPile->countCards());

    m_drawPile->printRemainingCards();

    //create discard pile and playedCards cache
    m_discardPile = new Deck(Deck::emptyDeck,0);
//    m_playedCards = new Deck(Deck::emptyDeck,0);
    m_playedCards = new std::vector<PlayedCard*>;
    qDebug() << "[GP] discard & playedCards piles created";

    //    PlayCardState* playCardState = new PlayCardState(NULL, "PlayCardState", 2);
    SwitchPlayerState* switchPlayerState = new SwitchPlayerState(NULL, NULL,StateMachine::switchPlayerState, PLAYCARDSTATEDURATION, attendingPlayer, NULL, m_playedCards, m_discardPile);
    PlayCardState* playCardState = new PlayCardState(NULL, NULL,StateMachine::playCardState, PLAYCARDSTATEDURATION, attendingPlayer, NULL, m_playedCards, NULL);
    GatherNopeState* gatherNopeState = new GatherNopeState(NULL, NULL,StateMachine::gatherNopeState, GATHERNOPESTATEDURATION, attendingPlayer, NULL, m_playedCards, NULL);
    ActivateCardState* activateCardState = new ActivateCardState(NULL, NULL,StateMachine::activateCardState, ACTIVATECARDSTATEDURATION, attendingPlayer, m_drawPile, m_playedCards, m_discardPile);
    DrawCardState* drawCardState = new DrawCardState(NULL, NULL,StateMachine::drawCardState, DRAWCARDSTATEDURATION, attendingPlayer, m_drawPile, NULL, NULL);
    DefuseCardState* defuseCardState = new DefuseCardState(NULL, NULL,StateMachine::defuseState, DEFUSECARDSTATEDURATION, attendingPlayer, NULL, m_playedCards, m_discardPile);
    PlaceKittenState* placeKittenState = new PlaceKittenState(NULL, NULL,StateMachine::placeKittenState, PLACEKITTENSTATEDURATION, attendingPlayer, m_drawPile, NULL, NULL);
    PlayerLostState* playerLostState = new PlayerLostState(NULL, NULL,StateMachine::playerLostState, PLAYERLOSTSTATEDURATION, attendingPlayer, m_drawPile, m_playedCards, m_discardPile);
    EndGameState* endGameState = new EndGameState(NULL, NULL,StateMachine::endGameState, ENDGAMESTATEDURATION, attendingPlayer, m_drawPile, m_playedCards, m_discardPile);



    qDebug() << "[GP] states constructed";

    //add states to tracker to delete them later
    stateTracker = new std::vector<BaseState*>();
    stateTracker->push_back(switchPlayerState); //must be at first position !!!!!!!!!!!
    stateTracker->push_back(playCardState);
    stateTracker->push_back(gatherNopeState);
    stateTracker->push_back(activateCardState);
    stateTracker->push_back(drawCardState);
    stateTracker->push_back(defuseCardState);
    stateTracker->push_back(placeKittenState);
    stateTracker->push_back(playerLostState);
    stateTracker->push_back(endGameState);  //must be at last position !!!!!

    //set next states
    switchPlayerState->setNextState_A(playCardState);


    playCardState->setNextState_A(drawCardState);           //no card played
    playCardState->setNextState_B(gatherNopeState);         //card played


    gatherNopeState->setNextState_A(activateCardState);     //not noped
    gatherNopeState->setNextState_B(playCardState);         //noped

    activateCardState->setNextState_A(playCardState);       //standart transition
    activateCardState->setNextState_B(switchPlayerState);   //on skip and attack


    drawCardState->setNextState_A(switchPlayerState);       //no kitten drawn
    drawCardState->setNextState_B(defuseCardState);         //kitten drawn

    defuseCardState->setNextState_A(playerLostState);       //not defused
    defuseCardState->setNextState_B(placeKittenState);      //defused

    placeKittenState->setNextState_A(switchPlayerState);    //only one transition

    playerLostState->setNextState_A(switchPlayerState);     //more then 2 players left
    playerLostState->setNextState_B(endGameState);          //only the winner left

    qDebug() << "[GP] transitions constructed";



    timer = new QTimer(this);


//    BaseState::activePlayer = -1;

/*
 *
 * Signal connections from here:
 *
 * */



    //connect Timer Signal to updateState Method in StateMachine
    bool cn = connect(timer, SIGNAL(timeout()), this, SLOT(slt_updateState())); //checked
    qDebug() << "[GP] connection to Timer success ? " << cn;



    //signals from all states here

    //connect changedState signals from all states ....
    for(auto bla = stateTracker->begin(); bla != stateTracker->end(); ++bla)
    {
        //connection to get the new ref time for the stateMachine -> IMPORTANT ! DONT DELETE !
        bool chgStateToThis = connect(*bla, SIGNAL(sig_changedState(BaseState::playStates)), this, SLOT(slt_changedState(BaseState::playStates)));  //checked
        qDebug() << "[GP] connection " <<  (*bla)->getName() << "to gameplay ? " << chgStateToThis;

        //connection to network for sending new state over network for testing
        bool chgStateToNet = connect(*bla, SIGNAL(sig_changedState(BaseState::playStates)), server, SLOT(stateChanged(BaseState::playStates))); //checked
        qDebug() << "[GP] connection " <<  (*bla)->getName() << "to server ? " << chgStateToNet;

        //connect timer for client
        connect(*bla, SIGNAL(sig_refreshTimer(double)), server, SLOT(slt_sendProgress(double)));

    }


    //signals only in certain states from here

    //assign state to statemachine and connect its signal so its only accessable when the state is assigned to the statemachine -> convinient !


    stateMachine = switchPlayerState;

    //connect demoSwitchPlayerState

    //connect the switchplayer signal to network

    bool sps1 = connect(stateMachine, SIGNAL(sigSendActivePlayer(QString)), server, SLOT(sendActivePlayer(QString)));   //connected
    qDebug() << "[GP] sps1 ? " << sps1;





    stateMachine = playCardState;

    //connect PlayCardStates

    //state signals
    //connect player played card to server so it can be sent to all other players
    bool pcs1 = connect(stateMachine, SIGNAL(sig_playerPlayedCard(QString,std::vector<AbstractCard::CardTypes>)), server, SLOT(slt_sendPlayedCards(QString,std::vector<AbstractCard::CardTypes>))); //checked
    qDebug() << "[GP] pcs1 ? " << pcs1;


    //state slots
    //connect server received card from player to playedCard state - if its empty, no card wants to be played
    bool pcs2 = connect (server,SIGNAL(clientPlayedCardsReceived(QString,std::vector<AbstractCard::CardTypes>)), stateMachine, SLOT(slt_playedCardsReceived(QString,std::vector<AbstractCard::CardTypes>)));    //checked
    qDebug() << "[GP] pcs2 ? " << pcs2;

    bool pcs3 = connect(stateMachine, SIGNAL(sig_amountOfCardsChanged(QString,uint)), server, SLOT(slt_amountOfCardsChanged(QString,uint)));    //checked
    qDebug() << "[GP] pcs3 ? " << pcs3;





    stateMachine = gatherNopeState;

    //connect gatherNopeState

    bool gns1 = connect (server,SIGNAL(clientPlayedCardsReceived(QString,std::vector<AbstractCard::CardTypes>)), stateMachine, SLOT(slt_playedCardsReceived(QString,std::vector<AbstractCard::CardTypes>)));    //checked
    qDebug() << "[GP] gns1 ? " << gns1;

    bool gns2 = connect(stateMachine, SIGNAL(sig_amountOfCardsChanged(QString,uint)), server, SLOT(slt_amountOfCardsChanged(QString,uint)));    //checked
    qDebug() << "[GP] gns2 ? " << gns2;

    connect(stateMachine, SIGNAL(sig_playerPlayedCard(QString,std::vector<AbstractCard::CardTypes>)), server, SLOT(slt_sendPlayedCards(QString,std::vector<AbstractCard::CardTypes>))); //checked




    stateMachine = activateCardState;

    bool acs1 = connect(stateMachine, SIGNAL(sig_sendSeeTheFutureCards(QString, std::vector<AbstractCard::CardTypes>)), server, SLOT(slt_sendSeeTheFutureCards(QString,std::vector<AbstractCard::CardTypes>))); //checked
    qDebug() << "[GP] acs1 ? " << acs1;

    connect(stateMachine, SIGNAL(sig_playerLostCard(QString, std::vector<AbstractCard::CardTypes>)), server, SLOT(slt_removeCards(QString, std::vector<AbstractCard::CardTypes>)));

    connect(stateMachine, SIGNAL(sig_playerDrawCard(QString, std::vector<AbstractCard::CardTypes>)), server, SLOT(slt_sendCards(QString,std::vector<AbstractCard::CardTypes>)));

    connect(stateMachine, SIGNAL(sig_shuffleDeck()), server, SLOT(slt_sendDeckShuffled()));

    connect(stateMachine, SIGNAL(sig_amountOfCardsChanged(QString,uint)), server, SLOT(slt_amountOfCardsChanged(QString,uint)));

    stateMachine = drawCardState;

    //connect drawCardState

    //state signals
    //connect player played card to server so it can be sent to all other players
    bool dcs1 = connect(stateMachine, SIGNAL(sig_playerDrawCard(QString, std::vector<AbstractCard::CardTypes>)), server, SLOT(slt_sendCards(QString,std::vector<AbstractCard::CardTypes>)));    //checked
    qDebug() << "[GP] dcs1 ? " << dcs1;

    bool dcs2 = connect(stateMachine, SIGNAL(sig_amountOfCardsChanged(QString,uint)), server, SLOT(slt_amountOfCardsChanged(QString,uint)));    //checked
    qDebug() << "[GP] dcs2 ? " << dcs2;

    bool dcs3 = connect(stateMachine, SIGNAL(sig_drawPileAmountChanged(uint)), server, SLOT(slt_drawPileCountChanged(uint)));    //checked
    qDebug() << "[GP] dcs3 ? " << dcs3;



    stateMachine = defuseCardState;

    //connect defuseCardState

    //connect server received card from player to playedCard state - if its empty, no card wants to be played
    bool dfs1 = connect (server,SIGNAL(clientPlayedCardsReceived(QString,std::vector<AbstractCard::CardTypes>)), stateMachine, SLOT(slt_playedCardsReceived(QString,std::vector<AbstractCard::CardTypes>)));    //checked
    qDebug() << "[GP] dfs1 ? " << dfs1;

    bool dfs2 = connect(stateMachine, SIGNAL(sig_amountOfCardsChanged(QString,uint)), server, SLOT(slt_amountOfCardsChanged(QString,uint)));    //checked
    qDebug() << "[GP] dfs2 ? " << dfs2;

    connect(stateMachine, SIGNAL(sig_playerPlayedCard(QString,std::vector<AbstractCard::CardTypes>)), server, SLOT(slt_sendPlayedCards(QString,std::vector<AbstractCard::CardTypes>))); //checked


    stateMachine = placeKittenState;

    //connect placeKittenState

    bool pks1 = connect (server,SIGNAL(sig_placeKitten(QString,int)), stateMachine, SLOT(slt_placeKittenReceived(QString, int)));   //checked
    qDebug() << "[GP] pks1 ? " << pks1;

    bool pks2 = connect(stateMachine, SIGNAL(sig_drawPileAmountChanged(uint)), server, SLOT(slt_drawPileCountChanged(uint)));    //checked
    qDebug() << "[GP] pks2 ? " << pks2;


    stateMachine = playerLostState;

    //connect playerLostState

    bool pls1 = connect(stateMachine, SIGNAL(sig_playerLost(QString)), server, SLOT(announceLooser(QString)));  //checked
    qDebug() << "[GP] pls1 ? " << pls1;

    stateMachine = endGameState;

    //connect endGameState

    bool egs1 = connect(stateMachine, SIGNAL(sig_announceWinner(QString)), server, SLOT(announceWinner(QString)));  //checked
    qDebug() << "[GP] egs1 ? " << egs1;

    bool egs2 = connect(stateMachine, SIGNAL(sig_gameEnded()), server, SLOT(slt_sendGameEnded()));  //checked
    qDebug() << "[GP] egs2 ? " << egs2;

    bool egs3 = connect(stateMachine, SIGNAL(sig_gameEnded()), this, SLOT(slt_gameEnded()));    //checked
    qDebug() << "[GP] egs3 ? " << egs3;


    connect(stateMachine, SIGNAL(sig_sendPlayerStatistics(QString,QMap<Logging::logVals,uint>,QString)), server, SLOT(slt_sendPlayerStatistics(QString,QMap<Logging::logVals,uint>,QString)));
    connect(stateMachine, SIGNAL(sig_sendGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int>>)), server, SLOT(slt_sendGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int>>)));

    //set stateMachine to startState
    stateMachine = switchPlayerState;


    timer->start(1000);
    qDebug() << "[GP] Startet " << stateMachine->getName();
}

void hostGameplay::assignServer(Server *passedServer){
    server = passedServer;
}

void hostGameplay::sendBufferToNetwork(QString ip)
{
    //due to architecture, a vector must be sent

    std::vector<AbstractCard::CardTypes> sendelicious;

    for(auto bla=cardBuffer->begin(); bla != cardBuffer->end(); ++bla){
        sendelicious.push_back(*bla);
    }

    emit deckCreatedSendToPlayer(ip, sendelicious);
}

hostGameplay::~hostGameplay()
{

    delete m_discardPile;
    delete m_drawPile;
    delete m_playedCards;


    delete timer;
    delete stateTracker;
    delete stateMachine;

//    qDebug()<< "[DISCONNECT HOSTGP FROM SERVER]" << disconnect(this, 0, server, 0);
//    qDebug()<< "[DISCONNECT HOSTGP FROM SERVER]" << disconnect(server, 0, this, 0);

    //as an object living inside gameplay it lifecycle ends when gameplay ends.
    //killing a vector will implicitly call the desturictors of its nested object
}

//timer method
void hostGameplay::slt_updateState(){

//    qDebug() << "[GP] Check stateMachine: " << stateMachine;


    ++elapsedTime;  //just the time for stateChange comparison with TimeStamp which is reset to actual time after every new stateSet

    //set timestamp on stateChange
    //timestamp = time when statechange happened
    //elapsedTime = time on every update
    stateMachine = stateMachine->update(timeStamp, elapsedTime);    //sending ref to self and timestamp when last state switch. update uses signal to change active state


}

//helper method getting actual time
int hostGameplay::getSystemTime(){
    return QTime::currentTime().msec();
}

void hostGameplay::removeAllClients()
{
    for (auto iter = attendingPlayer->begin(); iter != attendingPlayer->end();){

        //1. close socket
        qDebug() << "[GP]remove player: " << (*iter)->getIPAddress() << attendingPlayer->size();
        server->kickPlayer((*iter)->getIPAddress());

        //2. remove from attenting player list
        iter = attendingPlayer->erase(iter);
    }

    // shutdown game
    slt_gameEnded();
}

//****************************
//***********slots************
//****************************


void hostGameplay::slt_gameEnded(){
    qDebug() << "[GP] game ended Signal received";

    //stop timer
    timer->stop();

    //cleanup in adapter ?


    emit gameEndedSetNewGameReady();    //here destructor is called
}

void hostGameplay::slt_changedState(BaseState::playStates newState){
    qDebug() << "[GP] changedState Signal received";

    //set for inState time comparison to determine if state needs to be changed
    timeStamp = elapsedTime;
}

void hostGameplay::slt_playerLeftGame(QString ip)
{
    unsigned int positionOfLeaver = 0;

    // 0. did host leave ?
    if (ip == "::ffff:127.0.0.1"){
        qDebug() << "[GP] host left game, session terminating";

        //kick all players and shutdown
        removeAllClients();
    }
    else{
        qDebug() << "[GP] player " << ip << " left game, session refreshing";

        //1. pause timer
        timer->stop();
        emit sig_gamePaused();

        //2. revert cards who where played
        //2.1. iterate ofer m_playedCards and sent them to their owners
        qDebug() << "[GP] restore cards";
        for (auto iter = m_playedCards->begin(); iter != m_playedCards->end(); ++iter){
            server->slt_sendCard((*iter)->getOwnerIP(), (*iter)->getCardType());
        }

        //3.remove player from the attenting players list
        for (auto iter = attendingPlayer->begin(); iter != attendingPlayer->end();){

            //find player in player list
            if ((*iter)->getIPAddress() == ip){
                qDebug() << "[GP] remove leaver " << (*iter)->getIPAddress();
                iter = attendingPlayer->erase(iter);
                break;
            }
            else{
                ++iter;
                ++positionOfLeaver;
            }
        }


        //4. send the new list to the player

    //    server->sendPlayerList(attendingPlayer);
    //    will be done in gameAdapter


        qDebug() << "[GP] CHECK FOR LAST ACTIVE PLAYER";

        int playersLeft = 0;
        for(auto iter = attendingPlayer->begin(); iter != attendingPlayer->end(); ++iter){
            if(!((*iter)->getPlayerLost()))
                ++playersLeft;
        }

        if (playersLeft > 1){
            qDebug() << "[GP] 2+ players left, game resuming";
            //remove one expl kitten from the draw pile
            m_drawPile->adjustDeckBecauseofPlayerLeave(attendingPlayer->size());

            //shuffle cards (signal to clients)
            //shuffle is done when deck is adjusted
            server->slt_sendDeckShuffled();

            //5. set sm to switch player state
            stateMachine = stateTracker->at(0); //first is switchPlayerState
            //5.1 if leaver is active player, next players turn. Otherwise the activ player begins again
            stateMachine->setGameRestartedBecauseOfLeave(positionOfLeaver);
        }
        else{
            qDebug() << "[GP] 1 player left, end game, announce winner ;)";
            //5. set sm to switch player state
            stateMachine = stateTracker->back(); //last is endGameState
        }

        qDebug() << "[GP] set state to " << stateMachine->getName();

        //n. resume game
        timer->start();

        qDebug() << "[GP] resuming";
    }
}


/*
 *
 * Client from here
 *
 * */

//void clientGamePlay::assignClient(Client *passedClient)
//{
//    client = passedClient;
//}

//void clientGamePlay::playCard(AbstractCard::CardTypes card)
//{
//    qDebug() << "checking if player can send a card";
//    //check if player is eligable to play a card
//    if (m_canPlay){
//        cardsToBeSent.push_back(card);
//        qDebug() << "success, sending: " << cardsToBeSent.at(0);
//        emit sglSendCardToNetwork(cardsToBeSent);
//    }
//}

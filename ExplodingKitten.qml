import QtQuick 2.0
import CardTypes 1.0
import SoundTypes 1.0

Item {
    id: explodingKitten
    property alias msg: msg

    anchors.right: parent.right
    anchors.rightMargin: 120
    anchors.left: parent.left
    anchors.leftMargin: 120
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 120
    anchors.top: parent.top
    anchors.topMargin: 120

    Component.onCompleted: {
        //
        console.log("[EXPK] " + gameAdapter.qmlAmountDefuseCards);
        if(gameAdapter.qmlAmountDefuseCards != 0) {
            defuse.visible = true;
        } else {
            exitrect.visible = true;
        }
    }

    Rectangle {
        id: background
        color: "#DD000000"
        anchors.fill: parent

        Text {
            id: msg
            color: "#ffffff"
            text: qsTr("Exploding Kitten drawn!") + gameAdapter.translation
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.top: parent.top
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
        }

        Rectangle {
            id: kitten
            y: 1
            width: parent.width / 2
            color: "#00000000"
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: msg.bottom
            anchors.bottom: parent.bottom
            anchors.topMargin: 0
            anchors.bottomMargin: 0

            Card {
                id: kittencard
                anchors.fill: parent
                image.source: "qrc:///ExplodingKittenCard"
                countRect.visible: false
                rotationAnim.duration: 2000
                rotationAnim.running: true
            }
        }

        Rectangle {
            id: exitrect
            x: 0
            width: 20
            height: 20
            color: "#99ffffff"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            visible: false

            Text {
                id: exittext
                x: 0
                y: 0
                width: 20
                height: 20
                color: "#fb0000"
                text: "X"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 12
            }

            MouseArea {
                id: clickArea
                anchors.fill: parent
                onClicked: explodingKitten.destroy()
            }
        }

        Rectangle {
            id: defuse
            x: 4
            y: -7
            width: parent.width / 2
            color: "#00000000"
            visible: false
            anchors.right: parent.right
            anchors.top: msg.bottom
            anchors.rightMargin: 0

            Rectangle {
                id: sendButton1
                height: 22
                color: "#ffffff"
                visible: true
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter

                Text {
                    id: text2
                    text: qsTr("Defuse!") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    font.pixelSize: 20
                }

                MouseArea {
                    id: mouseArea2
                    anchors.fill: parent
                    onClicked: {
                        gameAdapter.slt_addCardToBePlayed(CardType.Defuse);
                        gameAdapter.playCards();
                        explodingKitten.destroy();
                    }
                }
                anchors.topMargin: 20
            }
            anchors.bottom: parent.bottom
            anchors.topMargin: 0
            anchors.bottomMargin: 0
        }

    }


}

import QtQuick 2.0
import CardTypes 1.0

Item {
    id: cardsToBePlayedItem

    property alias defuseCard : defuseCard
    property alias attackCard : attackCard
    property alias favorCard : favorCard
    property alias nopeCard : nopeCard
    property alias skipCard : skipCard
    property alias shuffleCard : shuffleCard
    property alias seeTheFutureCard : seeTheFutureCard
    property alias catsSchroedingerCard : catsSchroedingerCard
    property alias mommaCatCard : mommaCatCard
    property alias shyBladderCatCard : shyBladderCatCard
    property alias zombieCatCard : zombieCatCard
    property alias bikiniCatCard : bikiniCatCard

    /*
        \qmlmethod playSelectedCards()

        This method sends the buffered cards to server and hides every card and the form itself
    */
    function playSelectedCards() {
        gameAdapter.playCards();
        hide();

        //reset cards
        //not the best, nor the pretiest solution
        defuseCard.resetCount();
        defuseCard.hide();

        attackCard.resetCount();
        attackCard.hide();

        favorCard.resetCount();
        favorCard.hide();

        nopeCard.resetCount();
        nopeCard.hide();

        skipCard.resetCount();
        skipCard.hide();

        shuffleCard.resetCount();
        shuffleCard.hide();

        seeTheFutureCard.resetCount();
        seeTheFutureCard.hide();

        catsSchroedingerCard.resetCount();
        catsSchroedingerCard.hide();

        mommaCatCard.resetCount();
        mommaCatCard.hide();

        shyBladderCatCard.resetCount();
        shyBladderCatCard.hide();

        zombieCatCard.resetCount();
        zombieCatCard.hide();

        bikiniCatCard.resetCount();
        bikiniCatCard.hide();
    }

    /*
        \qmlmethod hide()

        This method hide the whole form
    */
    function hide() {
        cardsToBePlayedItem.visible = false;
    }

    /*
        \qmlmethod deselectCard(type, name)

        This method removes the selected card from to be played buffer and decrements its count
    */
    function deselectCard(type, name) {
        if(!type.countEmpty()) {
            type.decrementCount();
            if(name === "defuseCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.Defuse);
            }
            if(name === "attackCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.Attack);
            }
            if(name === "favorCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.Favor);
            }
            if(name === "nopeCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.Nope);
            }
            if(name === "skipCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.Skip);
            }
            if(name === "shuffleCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.Shuffle);
            }
            if(name === "seeTheFutureCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.SeeTheFuture);
            }
            if(name === "catsSchroedingerCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.CatsSchroedinger);
            }
            if(name === "mommaCatCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.MommaCat);
            }
            if(name === "shyBladderCatCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.ShyBladder);
            }
            if(name === "zombieCatCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.ZombieCat);
            }
            if(name === "bikiniCatCard") {
                gameAdapter.slt_removeCardToBePlayed(CardType.BikiniCat);
            }

            if(type.countEmpty()) {
                type.hide();
            }
        }
    }
    Rectangle {
        id: rectangle1
        color: "#99000000"
        anchors.fill: parent

        MouseArea {
            id: clickBlocker
            anchors.fill: parent
            hoverEnabled: true
        }
    }


    Flickable {
        id: flickable1
        width: parent.width
        height: parent.height
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.top: parent.top
        contentWidth: row2.width
        visible: true
        interactive: true
        contentHeight: parent.height
        boundsBehavior: Flickable.DragAndOvershootBounds
        flickableDirection: Flickable.HorizontalFlick

        Row {
            id: row2
            width: childrenRect.width
            height: parent.height
            spacing: 20
            anchors.left: parent.left
            anchors.leftMargin: 0

            Card {
                id: defuseCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                mouseArea1.onClicked: deselectCard(this, "defuseCard")
                visible: false
            }

            Card {
                id: attackCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///attackCard"
                mouseArea1.onClicked: deselectCard(this, "attackCard")
                visible: false
            }

            Card {
                id: favorCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///favorCard"
                mouseArea1.onClicked: deselectCard(this, "favorCard")
                visible: false
            }

            Card {
                id: nopeCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///nopeCard"
                mouseArea1.onClicked: deselectCard(this, "nopeCard")
                visible: false
            }

            Card {
                id: skipCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///skipCard"
                mouseArea1.onClicked: deselectCard(this, "skipCard")
                visible: false
            }

            Card {
                id: shuffleCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///shuffleCard"
                mouseArea1.onClicked: deselectCard(this, "shuffleCard")
                visible: false
            }

            Card {
                id: seeTheFutureCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///seeTheFutureCard"
                mouseArea1.onClicked: deselectCard(this, "seeTheFutureCard")
                visible: false
            }

            Card {
                id: catsSchroedingerCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///catsSchroedingerCard"
                mouseArea1.onClicked: deselectCard(this, "catsSchroedingerCard")
                visible: false
            }

            Card {
                id: mommaCatCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///mommaCatCard"
                mouseArea1.onClicked: deselectCard(this, "mommaCatCard")
                visible: false
            }

            Card {
                id: shyBladderCatCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///shyBladderCatCard"
                mouseArea1.onClicked: deselectCard(this, "shyBladderCatCard")
                visible: false
            }

            Card {
                id: zombieCatCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///zombieCatCard"
                mouseArea1.onClicked: deselectCard(this, "zombieCatCard")
                visible: false
            }

            Card {
                id: bikiniCatCard
                width: 180
                height: parent.height * 0.8
                anchors.verticalCenter: parent.verticalCenter
                image.source: "qrc:///bikiniCatCard"
                mouseArea1.onClicked: deselectCard(this, "bikiniCatCard")
                visible: false
            }
        }
    }


    Rectangle {
        id: playCardsButton
        x: 0
        y: 0
        width: 200
        height: 47
        color: "#ffffff"

        Text {
            id: text1
            text: qsTr("Play Selected") + gameAdapter.translation
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            font.pixelSize: 12
        }

        MouseArea {
            id: mouseArea1
            anchors.fill: parent
            onClicked: playSelectedCards()
        }

    }
}

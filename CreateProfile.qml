import QtQuick 2.0

Item {
    id: createprofile
    property alias msg: msg

    anchors.right: parent.right
    anchors.rightMargin: 120
    anchors.left: parent.left
    anchors.leftMargin: 120
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 120
    anchors.top: parent.top
    anchors.topMargin: 120

    Rectangle {
        id: background
        color: "#DD000000"
        anchors.fill: parent

        Text {
            id: msg
            color: "#ededed"
            text: qsTr("Choose a Nickname:") + gameAdapter.translation
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.top: parent.top
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
        }

        Rectangle {
            id: nameWrapper
            color: "#00000000"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: msg.bottom
            anchors.topMargin: 0

            Rectangle {
                id: nameBG
                y: 0
                width: 360
                height: childrenRect.height
                color: "#000000"
                border.color: "#ffffff"
                anchors.right: parent.right
                anchors.rightMargin: 140
                anchors.left: parent.left
                anchors.leftMargin: 140
                anchors.verticalCenter: parent.verticalCenter

                TextInput {
                    id: name
                    y: 216
                    height: 22
                    color: "#ffffff"
                    text: qsTr("PLAYER") + gameAdapter.translation
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    horizontalAlignment: Text.AlignHCenter
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 20
                }
            }

            Rectangle {
                id: createButton
                height: 22
                color: "#ffffff"
                anchors.right: parent.right
                anchors.rightMargin: 140
                anchors.left: parent.left
                anchors.leftMargin: 140
                anchors.top: nameBG.bottom
                anchors.topMargin: 20

                Text {
                    id: text1
                    text: qsTr("Create") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    font.pixelSize: 20
                }

                MouseArea {
                    id: mouseArea1
                    anchors.fill: parent
                    onClicked: {
                        if(gameAdapter.createLocalProfile(name.text)) {
                            console.log(gameAdapter.getActiveProfile());
                            main_form.updateStatsTab();
                            createprofile.destroy();
                        }
                    }
                }
            }
        }

    }


}
